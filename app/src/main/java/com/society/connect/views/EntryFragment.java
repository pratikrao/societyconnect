package com.society.connect.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.society.connect.R;
import com.society.connect.adapter.SubMenuAdapter;
import com.society.connect.classes.BasicData;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EntryFragment extends Fragment {


    private RecyclerView listview_menu;
    private SubMenuAdapter adapter;
    private ArrayList<BasicData> submenus;

    public EntryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_entry, container, false);
        // Inflate the layout for this fragment
        listview_menu = (RecyclerView) v.findViewById(R.id.listview_menu);

        //POPULATE DETAILS
        submenus = new ArrayList<>();
        submenus.add(new BasicData("Complaints").setIcon(R.drawable.ic_comment_white_36dp).setAccent_color("#C2185B"));
        submenus.add(new BasicData("Payment Intimation").setIcon(R.drawable.ic_assessment_white_36dp).setAccent_color("#C2185B"));
        submenus.add(new BasicData("Booking").setIcon(R.drawable.ic_check_circle_white_36dp).setAccent_color("#C2185B"));
        submenus.add(new BasicData("View Booking").setIcon(R.drawable.ic_date_range_white_36dp).setAccent_color("#C2185B"));

        adapter = new SubMenuAdapter(getActivity(), submenus);
        listview_menu.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(), 1));
        listview_menu.setAdapter(adapter);
        return v;
    }

}
