package com.society.connect.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.society.connect.R;
import com.society.connect.adapter.ShortcutsAdapter;
import com.society.connect.classes.BasicData;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment03 extends Fragment {


    private RecyclerView shortcut_list;
    private ShortcutsAdapter shortcutAdapter;
    private ArrayList<BasicData> shortcutData;

    public DashboardFragment03() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard03, container, false);
        // Inflate the layout for this fragment
        shortcut_list = (RecyclerView) v.findViewById(R.id.list_shortcut);

        shortcutData = new ArrayList<>();


        shortcutData.add(new BasicData("E-LOCKER").setIcon(R.drawable.ic_lock_white_36dp));
        shortcutData.add(new BasicData("SOCIAL").setIcon(R.drawable.ic_group_white_36dp));
        shortcutData.add(new BasicData("Complaints").setIcon(R.drawable.ic_comment_white_36dp));
        shortcutData.add(new BasicData("Payment Imitation").setIcon(R.drawable.ic_assessment_white_36dp).setAccent_color("#C2185B"));
        shortcutData.add(new BasicData("Booking").setIcon(R.drawable.ic_check_circle_white_36dp).setAccent_color("#C2185B"));
        shortcutData.add(new BasicData("View Booking").setIcon(R.drawable.ic_date_range_white_36dp).setAccent_color("#C2185B"));
        shortcutData.add(new BasicData("Bill").setIcon(R.drawable.ic_comment_white_36dp).setAccent_color("#00796B"));
        shortcutData.add(new BasicData("Booking").setIcon(R.drawable.ic_assessment_white_36dp).setAccent_color("#00796B"));
        shortcutData.add(new BasicData("Booking History").setIcon(R.drawable.ic_check_circle_white_36dp).setAccent_color("#00796B"));
        shortcutData.add(new BasicData("Ledger").setIcon(R.drawable.ic_date_range_white_36dp).setAccent_color("#00796B"));

        shortcutAdapter = new ShortcutsAdapter(getActivity(), shortcutData);
        shortcutAdapter.setTransparent(true);

        //POPULATE SHORTCUT DETAILS
        shortcut_list.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(), 2));
        shortcut_list.setAdapter(shortcutAdapter);
        return v;
    }

}
