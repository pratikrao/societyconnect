package com.society.connect.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.MainActivity;
import com.society.connect.R;
import com.society.connect.adapter.ShortcutsAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {


    private RecyclerView shortcut_list;
    private ShortcutsAdapter shortcutAdapter;
    private TextView txtUserName;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        // Inflate the layout for this fragment
        shortcut_list = (RecyclerView) v.findViewById(R.id.list_shortcut);
        shortcutAdapter = new ShortcutsAdapter(getActivity());
        txtUserName = (TextView) v.findViewById(R.id.txtUserName);

        if (MainActivity.mUserName != null) {
            txtUserName.setText(MainActivity.mUserName);
        }
        //POPULATE SHORTCUT DETAILS
        shortcut_list.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(), 2));
        shortcut_list.setAdapter(shortcutAdapter);
        return v;
    }

}
