package com.society.connect;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.activity.CreateComplaint;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.LoginRequestParamModel;
import com.society.connect.model.PaymentIntimationParamModel;
import com.society.connect.model.complain_history.ComplainHistoryResponseModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.society.connect.R.id.edtName;
import static com.society.connect.R.id.edtPassword;
import static com.society.connect.R.id.txtDate;

/**
 * Created by administrator on 8/7/17.
 */

public class PaymentIntimationActivity extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient {

    private TextView txtTime;
    private EditText edtTransactionId, edtBranch, edtNarration, edtAmount;
    private Button btnSave;

    private Calendar calendar;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtTime.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_intimation);
        calendar = Calendar.getInstance();
        txtTime = (TextView) findViewById(R.id.txtTime);
        edtTransactionId = (EditText) findViewById(R.id.edtTransactionId);
        edtBranch = (EditText) findViewById(R.id.edtBranch);
        edtNarration = (EditText) findViewById(R.id.edtNarration);
        edtAmount = (EditText) findViewById(R.id.edtAmount);
        btnSave = (Button) findViewById(R.id.btnSave);

        txtTime.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("PAYMENT INTIMATION");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtTime:
                new DatePickerDialog(PaymentIntimationActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnSave:
                if (!TextUtils.isEmpty(edtTransactionId.getText().toString())) {
                    if (!TextUtils.isEmpty(edtAmount.getText().toString())) {
                        if (!TextUtils.isEmpty(txtTime.getText().toString())) {
                            PaymentIntimationParamModel loginRequestParamModel = new PaymentIntimationParamModel();
                            loginRequestParamModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
                            loginRequestParamModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
                            loginRequestParamModel.setPayDate(txtTime.getText().toString());
                            loginRequestParamModel.setTransactionNo(edtTransactionId.getText().toString());
                            if (!TextUtils.isEmpty(edtBranch.getText().toString())) {
                                loginRequestParamModel.setBankName(edtBranch.getText().toString());
                            } else {
                                loginRequestParamModel.setBankName("");
                            }
                            if (!TextUtils.isEmpty(edtNarration.getText().toString())) {
                                loginRequestParamModel.setNarration(edtNarration.getText().toString());
                            } else {
                                loginRequestParamModel.setNarration(edtNarration.getText().toString());
                            }
                            loginRequestParamModel.setAmount(edtAmount.getText().toString());
                            loginRequestParamModel.setCID(Default.CID);

                            if (NetworkHelper.isOnline(this)) {
                                Utils.showProgressDialog(this);
                                NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                                        APIMethod.PAYMENT_INTIMATION, APIRequestcode.PAYMENT_INTIMATION);
                                builder.addParameter("", new Gson().toJson(loginRequestParamModel));
                                builder.setContentType(NetworkRequest.ContentType.JSON);

                                NetworkRequest networkRequest = builder.build();
                                NetworkJob networkJob = new NetworkJob(this, networkRequest);
                                networkJob.execute();

                            } else {
                                Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(this, "Select Date", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Enter Amount", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(this, "Enter Transaction Id", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            ComplainHistoryResponseModel basicResponseModel = gson.fromJson(reader,
                    ComplainHistoryResponseModel.class);

            if (basicResponseModel.getSuccess()) {
                finish();
                Toast.makeText(this, "Submitted successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(PaymentIntimationActivity.this, basicResponseModel.getResult(), Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
