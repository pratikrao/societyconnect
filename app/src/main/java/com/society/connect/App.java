package com.society.connect;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by administrator on 8/6/17.
 */

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        //To Register the Shared Preference
        PreferencesManager.initializeInstance(this);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public boolean isLoggedIn() {
        return PreferencesManager.getInstance().getBooleanValue(Default.PREF_LOGGED_IN);
    }
}
