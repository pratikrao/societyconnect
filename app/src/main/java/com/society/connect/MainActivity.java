package com.society.connect;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.society.connect.activity.ChangePasswordActivity;
import com.society.connect.activity.LoginActivity;
import com.society.connect.activity.Profile;
import com.society.connect.classes.TabData;
import com.society.connect.views.DashboardFragment;
import com.society.connect.views.EntryFragment;
import com.society.connect.views.ReportFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    public static String mUserName;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ArrayList<TabData> tabList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mUserName = PreferencesManager.getInstance().getStringValue(Default.PREF_USERNAME);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        tabList = new ArrayList<>();

        //POPULATE TABS DETAILS
        tabList.add(new TabData("Dashboard", new DashboardFragment()).setColor(Color.parseColor("#303F9F")));
        tabList.add(new TabData("Entry", new EntryFragment()).setColor(Color.parseColor("#C2185B")));
        tabList.add(new TabData("Reports", new ReportFragment()).setColor(Color.parseColor("#00796B")));//E64A19 607D8B


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        //initialize color for tab
        tabLayout.setBackgroundColor(tabList.get(0).getColor());
        toolbar.setBackgroundColor(tabList.get(0).getColor());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(tabList.get(0).getColor());
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.setBackgroundColor(tabList.get(position).getColor());
                toolbar.setBackgroundColor(tabList.get(position).getColor());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(tabList.get(position).getColor());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_logout:
                PreferencesManager.getInstance().clear();
                startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;
            case R.id.action_profile:
                startActivity(new Intent(this, Profile.class));
                break;
            case R.id.action_change_pswd:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                break;
        }


        return super.onOptionsItemSelected(item);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return tabList.get(position).getPageLayout();
        }

        @Override
        public int getCount() {
            return tabList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position).getTitle();
        }
    }
}
