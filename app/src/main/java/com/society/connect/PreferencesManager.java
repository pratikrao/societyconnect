package com.society.connect;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Pratik
 * on 25/7/16.
 */
public class PreferencesManager {

    private static final String PREF_NAME = " com.society.connect";
    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public void initBootstrap() {
        if (!mPref.contains("isLogged")) {
            mPref.edit().putBoolean(Default.PREF_LOGGED_IN, false).putString(Default.PREF_ACODE, "").putString(Default.PREF_COCODE, "").putString(Default.PREF_USERNAME, "").apply();
        }
    }

    public void setStringValue(String KEY_VALUE, String value) {
        mPref.edit().putString(KEY_VALUE, value).apply();
    }

    public void setIntValue(String KEY_VALUE, int value) {
        mPref.edit().putLong(KEY_VALUE, value).apply();
    }

    public void setBooleanValue(String KEY_VALUE, boolean value) {
        mPref.edit().putBoolean(KEY_VALUE, value).apply();
    }


    public String getStringValue(String KEY_VALUE) {
        return mPref.getString(KEY_VALUE, "");
    }

    public long getIntValue(String KEY_VALUE) {
        return mPref.getLong(KEY_VALUE, 0);
    }

    public boolean getBooleanValue(String KEY_VALUE) {
        return mPref.getBoolean(KEY_VALUE, false);
    }

    public void remove(String key) {
        mPref.edit().remove(key).apply();
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}
