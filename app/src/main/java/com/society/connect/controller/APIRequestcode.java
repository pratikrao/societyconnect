package com.society.connect.controller;

/**
 * Created by administrator on 3/6/17.
 */

public class APIRequestcode {
    public static final int LOGIN = 1, LEDGER = 2, POST_COMPLAINT = 3,
            SHOW_BILL = 4, BILL_GRID = 5, LEDGER_GRID = 6,
            VIEW_COMPLAIN_HISTORY = 7, BOOKING_HEADER = 8, BOOKING_RATE = 9,
            POST_BOOKING = 10, VIEW_BOOKING = 11, CHECK_AVAILABILITY = 12, SOCIAL_CATEGORY = 13, PROFILE = 14, SOCIAL_CATEGORY_DETAILS = 15,
            PROFILE_UPDATE = 16, PAYMENT_INTIMATION = 17, PAYMENT_DETAILS = 18, PAYMENT_CONFIRM = 19, PAYMENT_RESPONSE = 20, VIEW_ELOCKER = 21;
}
