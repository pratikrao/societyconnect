package com.society.connect.controller;

/**
 * Created by administrator on 3/6/17.
 */

public class APIMethod {
    public static final String BASE_URL = "http://sampadasoftware.com/Vastuapp/MoApp.aspx/", // BASE URL
            LOGIN = BASE_URL + "GetLogin",// LOGIN SERVICE
            LEDGER = BASE_URL + "ShowLedger",
            POST_COMPLAINT = BASE_URL + "PostComplaints",
            SHOW_BILL = BASE_URL + "ShowBill",
            BILL_GRID = BASE_URL + "vShowBill",
            LEDGER_GRID = BASE_URL + "vShowLedger",
            VIEW_COMPLAIN_HISTORY = BASE_URL + "ViewComplaintHistory",
            BOOKING_HEADER = BASE_URL + "BookingDdlslList",
            BOOKING_RATE = BASE_URL + "ShowRate",
            POST_BOOKING = BASE_URL + "PostBooking",
            VIEW_BOOKING = BASE_URL + "ViewBooking",
            CHECK_AVAILABILITY = BASE_URL + "CheckBookingAvailability",
            PROFILE = BASE_URL + "GetMemberProfileDetails",
            SOCIAL_CATEGORY = BASE_URL + "GetCatagory",
            SOCIAL_CATEGORY_DETAILS = BASE_URL + "GetDetailsCatagoryWise",
            PROFILE_UPDATE = BASE_URL + "UpdateMemberProfile",
            PAYMENT_INTIMATION = BASE_URL + "PostPaymentIntimation",
            PAYMENT_DETAILS = BASE_URL + "PaymentMemberInfo",
            PAYMENT_CONFIRM = BASE_URL + "PostOLTP",
            PAYMENT_RESPONSE = BASE_URL + "PaymentResponse",
            VIEW_ELOCKER = BASE_URL + "ViewElocker";
}
