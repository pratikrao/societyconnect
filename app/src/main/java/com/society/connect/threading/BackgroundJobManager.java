package com.society.connect.threading;


/**
 * Created by yashesh on 6/8/2015.
 */
public class BackgroundJobManager<T extends BackgroundJob> {

    public static BackgroundJobManager instance;
    int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private BackgroundJobExecutor executor;

    private BackgroundJobManager() {
        executor = new BackgroundJobExecutor(NUMBER_OF_CORES * 2, NUMBER_OF_CORES * 2);
    }

    public static synchronized BackgroundJobManager getInstance() {
        if (instance == null) {
            instance = new BackgroundJobManager();
        }
        return instance;
    }

    public void submitJob(T job) {

        executor.execute(job);
    }
}
