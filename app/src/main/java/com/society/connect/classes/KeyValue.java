package com.society.connect.classes;

/**
 * Created by mohammed.rafique on 6/15/2017.
 */

public class KeyValue {
    String key, value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
