package com.society.connect.classes;

import android.graphics.Bitmap;

/**
 * Created by mohammed.rafique on 26/4/2017.
 */

public class BasicData {
    private String title;
    private int icon;
    private Bitmap thumbnail;
    private String description;
    private String accent_color;

    public BasicData(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public BasicData setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getIcon() {
        return icon;
    }

    public BasicData setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public BasicData setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public BasicData setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getAccent_color() {
        return accent_color;
    }

    public BasicData setAccent_color(String accent_color) {
        this.accent_color = accent_color;
        return this;
    }
}
