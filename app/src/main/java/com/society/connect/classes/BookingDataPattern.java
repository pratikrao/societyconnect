package com.society.connect.classes;

/**
 * Created by mohammed.rafique on 27/4/2017.
 */

public class BookingDataPattern {
    String name, status, date, remarks;

    public BookingDataPattern() {
    }

    public BookingDataPattern(String name, String status, String date, String remarks) {
        this.name = name;
        this.status = status;
        this.date = date;
        this.remarks = remarks;
    }

    public String getName() {
        return name;
    }

    public BookingDataPattern setName(String name) {
        this.name = name;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public BookingDataPattern setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getDate() {
        return date;
    }

    public BookingDataPattern setDate(String date) {
        this.date = date;
        return this;
    }

    public String getRemarks() {
        return remarks;
    }

    public BookingDataPattern setRemarks(String remarks) {
        this.remarks = remarks;
        return this;
    }
}
