package com.society.connect.classes;


import android.support.v4.app.Fragment;

import com.society.connect.R;

/**
 * Created by mohammed.rafique on 26/4/2017.
 */

public class TabData {
    public String title;
    public int icon;
    public int color;
    public Fragment pageLayout;


    public TabData(String title, Fragment pageLayout) {
        this.title = title;
        this.pageLayout = pageLayout;
        this.color = R.color.colorPrimary;
    }

    public String getTitle() {
        return title;
    }

    public TabData setTitle(String title) {
        this.title = title;
        return TabData.this;
    }

    public int getIcon() {
        return icon;
    }

    public TabData setIcon(int icon) {
        this.icon = icon;
        return TabData.this;
    }

    public int getColor() {
        return color;
    }

    public TabData setColor(int color) {
        this.color = color;
        return TabData.this;
    }

    public Fragment getPageLayout() {
        return pageLayout;
    }

    public TabData setPageLayout(Fragment pageLayout) {
        this.pageLayout = pageLayout;
        return TabData.this;
    }
}
