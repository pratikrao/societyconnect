package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 24/6/17.
 */

public class BookingHeaderResponseModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private Object result;
    @SerializedName("slotDATA")
    private List<SlotData> slotDATA = null;
    @SerializedName("headDATA")
    private List<HeadData> headDATA = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<SlotData> getSlotDATA() {
        return slotDATA;
    }

    public void setSlotDATA(List<SlotData> slotDATA) {
        this.slotDATA = slotDATA;
    }

    public List<HeadData> getHeadDATA() {
        return headDATA;
    }

    public void setHeadDATA(List<HeadData> headDATA) {
        this.headDATA = headDATA;
    }


}