package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 24/6/17.
 */

public class RateData {


    @SerializedName("slotName")
    private String slotName;
    @SerializedName("Rate")
    private String rate;


    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


}
