package com.society.connect.model;

import com.google.gson.annotations.SerializedName;
import com.society.connect.model.social.CategoryDataModel;

import java.util.List;

/**
 * Created by administrator on 10/7/17.
 */

public class PaymentConfirmationModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;

    public void setData(ConfirmationData data) {
        Data = data;
    }

    @SerializedName("Data")
    private ConfirmationData Data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ConfirmationData getData() {
        return Data;
    }
}
