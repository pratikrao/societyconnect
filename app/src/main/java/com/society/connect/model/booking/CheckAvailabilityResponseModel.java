package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 26/6/17.
 */

public class CheckAvailabilityResponseModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    @SerializedName("stat")
    private String stat;
}
