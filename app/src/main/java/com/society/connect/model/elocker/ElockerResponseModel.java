package com.society.connect.model.elocker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 10/7/17.
 */

public class ElockerResponseModel {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private Object result;
    @SerializedName("Data")
    private List<LockerData> data = null;

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public Object getResult() {
        return result;
    }


    public void setResult(Object result) {
        this.result = result;
    }

    public List<LockerData> getData() {
        return data;
    }

    public void setData(List<LockerData> data) {
        this.data = data;
    }



}
