package com.society.connect.model.booking.view_booking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 24/6/17.
 */

public class ViewBookingData {

    @SerializedName("date")
    private String date;
    @SerializedName("stat")
    private String stat;
    @SerializedName("remark")
    private String remark;
    @SerializedName("slotd")
    private String slotd;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSlotd() {
        return slotd;
    }

    public void setSlotd(String slotd) {
        this.slotd = slotd;
    }


}
