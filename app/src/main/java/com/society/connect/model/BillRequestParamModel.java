package com.society.connect.model;

/**
 * Created by administrator on 9/6/17.
 */

public class BillRequestParamModel {
    private String coCode;
    private String acode;
    private String moYr;
    private String month;
    private String year;

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getDate() {
        return moYr;
    }

    public void setDate(String date) {
        this.moYr = date;
    }
}
