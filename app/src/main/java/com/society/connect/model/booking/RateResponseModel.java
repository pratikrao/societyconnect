package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 24/6/17.
 */

public class RateResponseModel {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private Object result;
    @SerializedName("DATA")
    private List<RateData> dATA = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<RateData> getDATA() {
        return dATA;
    }

    public void setDATA(List<RateData> dATA) {
        this.dATA = dATA;
    }



}


