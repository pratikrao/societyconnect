package com.society.connect.model.bill;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class BillModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("Data")
    private BillData loginData;

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getResult() {
        return result;
    }


    public void setResult(String result) {
        this.result = result;
    }


    public BillData getLoginData() {
        return loginData;
    }


    public void setLoginData(BillData loginData) {
        this.loginData = loginData;
    }
}
