package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 24/6/17.
 */

public class HeadData {

    @SerializedName("code")
    private String code;
    @SerializedName("descr")
    private String descr;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

}
