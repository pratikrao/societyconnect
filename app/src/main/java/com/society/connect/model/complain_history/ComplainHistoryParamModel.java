package com.society.connect.model.complain_history;

/**
 * Created by administrator on 24/6/17.
 */

public class ComplainHistoryParamModel {
    private String coCode;

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getFrmDate() {
        return frmDate;
    }

    public void setFrmDate(String frmDate) {
        this.frmDate = frmDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    private String acode;
    private String frmDate;
    private String toDate;
    private String stat;

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;
}
