package com.society.connect.model.booking.view_booking;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 24/6/17.
 */

public class ViewBookingResponseModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private Object result;
    @SerializedName("Data")
    private List<ViewBookingData> data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }


    public List<ViewBookingData> getData() {
        return data;
    }


    public void setData(List<ViewBookingData> data) {
        this.data = data;
    }


}
