package com.society.connect.model.social;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rafique on 9/6/17.
 */

public class CategoryParamModel {

        private String coCode;
        private String acode;

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;


        public String getCoCode() {
            return coCode;
        }

        public void setCoCode(String coCode) {
            this.coCode = coCode;
        }

        public String getAcode() {
            return acode;
        }

        public void setAcode(String acode) {
            this.acode = acode;
        }



}
