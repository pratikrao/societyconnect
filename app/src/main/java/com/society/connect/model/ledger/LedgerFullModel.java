package com.society.connect.model.ledger;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerFullModel {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("creditAmount")
    private Object creditAmount;
    @SerializedName("debitAmount")
    private Object debitAmount;
    @SerializedName("openingBal")
    private String openingBal;
    @SerializedName("openingCrDr")
    private String openingCrDr;
    @SerializedName("closingBal")
    private String closingBal;
    @SerializedName("closingCrDr")
    private String closingCrDr;
    @SerializedName("Data")
    private List<LedgerFullData> data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Object getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Object creditAmount) {
        this.creditAmount = creditAmount;
    }

    public Object getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Object debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(String openingBal) {
        this.openingBal = openingBal;
    }

    public String getOpeningCrDr() {
        return openingCrDr;
    }

    public void setOpeningCrDr(String openingCrDr) {
        this.openingCrDr = openingCrDr;
    }

    public String getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(String closingBal) {
        this.closingBal = closingBal;
    }

    public String getClosingCrDr() {
        return closingCrDr;
    }

    public void setClosingCrDr(String closingCrDr) {
        this.closingCrDr = closingCrDr;
    }

    public List<LedgerFullData> getData() {
        return data;
    }

    public void setData(List<LedgerFullData> data) {
        this.data = data;
    }

}

