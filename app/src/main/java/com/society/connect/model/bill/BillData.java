package com.society.connect.model.bill;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class BillData {
    @SerializedName("billNumber")
    private String billNumber;
    @SerializedName("billDate")
    private String billDate;
    @SerializedName("currentInt")
    private Integer currentInt;
    @SerializedName("arrears")
    private Integer arrears;
    @SerializedName("totalAmount")
    private Integer totalAmount;
    @SerializedName("totalrowAmt")
    private Integer totalrowAmt;

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public Integer getCurrentInt() {
        return currentInt;
    }

    public void setCurrentInt(Integer currentInt) {
        this.currentInt = currentInt;
    }

    public Integer getArrears() {
        return arrears;
    }

    public void setArrears(Integer arrears) {
        this.arrears = arrears;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalrowAmt() {
        return totalrowAmt;
    }

    public void setTotalrowAmt(Integer totalrowAmt) {
        this.totalrowAmt = totalrowAmt;
    }

}
