package com.society.connect.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 8/7/17.
 */

public class PaymentDetailResposneModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("Data")
    private PaymentDetailsData data;
    @SerializedName("Mode")
    private List<ModeData> mode = null;


    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getResult() {
        return result;
    }


    public void setResult(String result) {
        this.result = result;
    }


    public PaymentDetailsData getData() {
        return data;
    }


    public void setData(PaymentDetailsData data) {
        this.data = data;
    }


    public List<ModeData> getMode() {
        return mode;
    }


    public void setMode(List<ModeData> mode) {
        this.mode = mode;
    }


}
