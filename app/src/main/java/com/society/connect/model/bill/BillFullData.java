package com.society.connect.model.bill;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class BillFullData {
    @SerializedName("BillAmt")
    private String BillAmt;
    @SerializedName("HeadName")
    private String HeadName;

    public String getAmount() {
        return BillAmt;
    }

    public BillFullData setAmount(String amount) {
        this.BillAmt = amount;
        return this;
    }

    public String getHeadName() {
        return HeadName;
    }


    public BillFullData setHeadName(String headName) {
        this.HeadName = headName;
        return this;

    }


}