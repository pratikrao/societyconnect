package com.society.connect.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 10/7/17.
 */

public class ConfirmationData {
    public String getMerchantTxnId() {
        return merchantTxnId;
    }

    public void setMerchantTxnId(String merchantTxnId) {
        this.merchantTxnId = merchantTxnId;
    }

    public String getSecureToken() {
        return secureToken;
    }

    public void setSecureToken(String secureToken) {
        this.secureToken = secureToken;
    }

    @SerializedName("transactionId")
    private String transactionId;
    @SerializedName("totalAmount")
    private String totalAmount;
    @SerializedName("merchantTxnId")
    private String merchantTxnId;
    @SerializedName("secureToken")
    private String secureToken;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    @SerializedName("charges")
    private String charges;
}
