package com.society.connect.model.ledger;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("Data")
    private LedgerData loginData;

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getResult() {
        return result;
    }


    public void setResult(String result) {
        this.result = result;
    }


    public LedgerData getLoginData() {
        return loginData;
    }


    public void setLoginData(LedgerData loginData) {
        this.loginData = loginData;
    }


}

