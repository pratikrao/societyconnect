package com.society.connect.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 8/7/17.
 */

public class PaymentDetailsData {
    @SerializedName("memberName")
    private String memberName;
    @SerializedName("cellNo")
    private String cellNo;
    @SerializedName("emailid")
    private String emailid;
    @SerializedName("amt")
    private String amt;
    @SerializedName("uid")
    private String uid;
    @SerializedName("pwd")
    private String pwd;
    @SerializedName("mid")
    private String mid;
    @SerializedName("mId")
    private String mId;
    @SerializedName("aggregatorID")
    private String aggregatorID;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    @SerializedName("appId")
    private String appId;
    @SerializedName("sid")
    private String sid;


    public String getMemberName() {
        return memberName;
    }


    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }


    public String getCellNo() {
        return cellNo;
    }


    public void setCellNo(String cellNo) {
        this.cellNo = cellNo;
    }


    public String getEmailid() {
        return emailid;
    }


    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }


    public String getAmt() {
        return amt;
    }


    public void setAmt(String amt) {
        this.amt = amt;
    }


    public String getUid() {
        return uid;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }


    public String getPwd() {
        return pwd;
    }


    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    public String getMid() {
        return mid;
    }


    public void setMid(String mid) {
        this.mid = mid;
    }


    public String getSid() {
        return sid;
    }


    public void setSid(String sid) {
        this.sid = sid;
    }


    public String getAggregatorID() {
        return aggregatorID;
    }

    public void setAggregatorID(String aggregatorID) {
        this.aggregatorID = aggregatorID;
    }
}
