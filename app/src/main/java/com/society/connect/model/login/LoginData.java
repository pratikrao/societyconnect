package com.society.connect.model.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 5/6/17.
 */

public class LoginData {

    @SerializedName("userName")
    private String userName;
    @SerializedName("password")
    private String password;
    @SerializedName("acode")
    private String acode;
    @SerializedName("coCode")
    private Integer coCode;
    @SerializedName("memberName")
    private String memberName;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getAcode() {
        return acode;
    }


    public void setAcode(String acode) {
        this.acode = acode;
    }


    public Integer getCoCode() {
        return coCode;
    }


    public void setCoCode(Integer coCode) {
        this.coCode = coCode;
    }


}
