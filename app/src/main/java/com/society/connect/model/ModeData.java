package com.society.connect.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 8/7/17.
 */

public class ModeData {
    @SerializedName("modeCode")
    private String modeCode;
    @SerializedName("descr")
    private String descr;


    public String getModeCode() {
        return modeCode;
    }


    public void setModeCode(String modeCode) {
        this.modeCode = modeCode;
    }


    public String getDescr() {
        return descr;
    }


    public void setDescr(String descr) {
        this.descr = descr;
    }


}
