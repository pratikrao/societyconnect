package com.society.connect.model.social;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mohammed.rafique on 7/4/2017.
 */

public class CategoryDataModel {

    @SerializedName("descr")
    private String descr;
    @SerializedName("code")
    private String code;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

}
