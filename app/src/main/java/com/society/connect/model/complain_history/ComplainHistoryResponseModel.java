package com.society.connect.model.complain_history;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 24/6/17.
 */

public class ComplainHistoryResponseModel {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("complaintNumber")
    private String complaintNumber;
    @SerializedName("Data")
    private List<ComplainHistoryData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }
    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<ComplainHistoryData> getData() {
        return data;
    }

    public void setData(List<ComplainHistoryData> data) {
        this.data = data;
    }


}