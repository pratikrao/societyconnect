package com.society.connect.model.booking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 24/6/17.
 */

public class SlotData {

    @SerializedName("slotCode")
    private String slotCode;
    @SerializedName("SlotName")
    private String slotName;


    public String getSlotCode() {
        return slotCode;
    }

    public void setSlotCode(String slotCode) {
        this.slotCode = slotCode;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }


}
