package com.society.connect.model.profile;

import com.google.gson.annotations.SerializedName;
import com.society.connect.model.social.CategoryDataModel;

import java.util.List;

/**
 * Created by mohammed.rafique on 7/4/2017.
 */

public class ProfileParamModel {
        public static class Request{

            private String coCode;
            private String acode;

            public String getCID() {
                return CID;
            }

            public void setCID(String CID) {
                this.CID = CID;
            }

            private String CID;

            public Request() {
            }

            public String getCoCode() {
                return coCode;
            }

            public void setCoCode(String coCode) {
                this.coCode = coCode;
            }

            public String getAcode() {
                return acode;
            }

            public void setAcode(String acode) {
                this.acode = acode;
            }
        }

        public static class Response{
            @SerializedName("success")
            private Boolean success;
            @SerializedName("result")
            private String result;
            @SerializedName("Data")
            private List<ProfileDataModel> Data = null;


            public Boolean getSuccess() {
                return success;
            }

            public void setSuccess(Boolean success) {
                this.success = success;
            }

            public String getResult() {
                return result;
            }

            public void setResult(String result) {
                this.result = result;
            }

            public List<ProfileDataModel> getData() {
                return Data;
            }

        }


        public static class UpdateRequest{
            private String coCode;
            private String acode;
            private String email;
            private String cellno;

            public String getCID() {
                return CID;
            }

            public void setCID(String CID) {
                this.CID = CID;
            }

            private String CID;


            public String getCoCode() {
                return coCode;
            }

            public void setCoCode(String coCode) {
                this.coCode = coCode;
            }

            public String getAcode() {
                return acode;
            }

            public void setAcode(String acode) {
                this.acode = acode;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getCellno() {
                return cellno;
            }

            public void setCellno(String cellno) {
                this.cellno = cellno;
            }
        }
        public static class UpdateResponse{
            @SerializedName("success")
            private Boolean success;
            @SerializedName("result")
            private String result;

            public Boolean getSuccess() {
                return success;
            }

            public void setSuccess(Boolean success) {
                this.success = success;
            }

            public String getResult() {
                return result;
            }

            public void setResult(String result) {
                this.result = result;
            }



        }


}
