package com.society.connect.model.social;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 5/7/17.
 */

public class SocialData {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCellNo() {
        return cellNo;
    }

    public void setCellNo(String cellNo) {
        this.cellNo = cellNo;
    }

    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("contactPerson")
    private String contactPerson;
    @SerializedName("cellNo")
    private String cellNo;

}
