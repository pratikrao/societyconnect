package com.society.connect.model;

import com.google.gson.annotations.SerializedName;
import com.society.connect.model.social.CategoryDataModel;

import java.util.List;

/**
 * Created by administrator on 6/7/17.
 */

public class CategoryResponseModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("Data")
    private List<CategoryDataModel> Data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<CategoryDataModel> getData() {
        return Data;
    }
}
