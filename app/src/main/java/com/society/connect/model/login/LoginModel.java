package com.society.connect.model.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 5/6/17.
 */

public class LoginModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("Data")
    private LoginData loginData;

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getResult() {
        return result;
    }


    public void setResult(String result) {
        this.result = result;
    }


    public LoginData getLoginData() {
        return loginData;
    }


    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }


}
