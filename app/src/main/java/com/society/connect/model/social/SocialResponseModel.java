package com.society.connect.model.social;

import com.google.gson.annotations.SerializedName;
import com.society.connect.model.booking.view_booking.ViewBookingData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 5/7/17.
 */

public class SocialResponseModel {

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<SocialData> getData() {
        return data;
    }

    public void setData(List<SocialData> data) {
        this.data = data;
    }

    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private Object result;
    @SerializedName("Data")
    private List<SocialData> data = null;

}
