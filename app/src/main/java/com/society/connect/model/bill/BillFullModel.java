package com.society.connect.model.bill;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by administrator on 9/6/17.
 */

public class BillFullModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("billDate")
    private String billDate;
    @SerializedName("billNumber")
    private String billNumber;
    @SerializedName("currentInt")
    private Integer currentInt;
    @SerializedName("arrears")
    private Integer arrears;
    @SerializedName("totalAmount")
    private Integer totalAmount;
    @SerializedName("totalrowAmt")
    private Integer totalrowAmt;
    @SerializedName("Data")
    private List<BillFullData> data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Integer getCurrentInt() {
        return currentInt;
    }

    public void setCurrentInt(Integer currentInt) {
        this.currentInt = currentInt;
    }

    public Integer getArrears() {
        return arrears;
    }

    public void setArrears(Integer arrears) {
        this.arrears = arrears;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }


    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalrowAmt() {
        return totalrowAmt;
    }

    public void setTotalrowAmt(Integer totalrowAmt) {
        this.totalrowAmt = totalrowAmt;
    }

    public List<BillFullData> getData() {
        return data;
    }

    public void setData(List<BillFullData> data) {
        this.data = data;
    }


}
