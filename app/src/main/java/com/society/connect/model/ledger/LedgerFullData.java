package com.society.connect.model.ledger;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerFullData {
    @SerializedName("doc_no")
    private String docNo;
    @SerializedName("doc_date")
    private String docDate;
    @SerializedName("type")
    private String type;
    @SerializedName("Descr")
    private String descr;
    @SerializedName("Debit")
    private String debit;
    @SerializedName("Credit")
    private String credit;
    @SerializedName("chqno")
    private String chqno;


    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getChqno() {
        return chqno;
    }

    public void setChqno(String chqno) {
        this.chqno = chqno;
    }


}
