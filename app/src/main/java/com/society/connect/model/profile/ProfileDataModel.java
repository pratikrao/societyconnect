package com.society.connect.model.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mohammed.rafique on 7/4/2017.
 */

public class ProfileDataModel {
    @SerializedName("bldgno")
    private String bldgno;
    @SerializedName("wing")
    private String wing;

    @SerializedName("flatno")
    private String flatno;
    @SerializedName("NAME")
    private String name;

    @SerializedName("FLATAREA")
    private String flatarea;
    @SerializedName("CELLNO")
    private String cellnumber;
    @SerializedName("MEMBER_ID")
    private String memberID;
    @SerializedName("EMAILID")
    private String emailID;
    @SerializedName("STATUS")
    private String status;

    public String getBldgno() {
        return bldgno;
    }

    public void setBldgno(String bldgno) {
        this.bldgno = bldgno;
    }

    public String getWing() {
        return wing;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    public String getFlatno() {
        return flatno;
    }

    public void setFlatno(String flatno) {
        this.flatno = flatno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlatarea() {
        return flatarea;
    }

    public void setFlatarea(String flatarea) {
        this.flatarea = flatarea;
    }

    public String getCellnumber() {
        return cellnumber;
    }

    public void setCellnumber(String cellnumber) {
        this.cellnumber = cellnumber;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
