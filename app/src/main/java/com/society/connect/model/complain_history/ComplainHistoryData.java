package com.society.connect.model.complain_history;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 24/6/17.
 */

public class ComplainHistoryData {

    @SerializedName("complaintNo")
    private String complaintNo;
    @SerializedName("solvedDt")
    private String solvedDt;
    @SerializedName("solvedBy")
    private String solvedBy;
    @SerializedName("stat")
    private String stat;
    @SerializedName("remark")
    private String remark;

    public String getComplaintNo() {
        return complaintNo;
    }

    public void setComplaintNo(String complaintNo) {
        this.complaintNo = complaintNo;
    }

    public String getSolvedDt() {
        return solvedDt;
    }

    public void setSolvedDt(String solvedDt) {
        this.solvedDt = solvedDt;
    }

    public String getSolvedBy() {
        return solvedBy;
    }

    public void setSolvedBy(String solvedBy) {
        this.solvedBy = solvedBy;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
