package com.society.connect.model.booking;

/**
 * Created by administrator on 24/6/17.
 */

public class BookingHeadParamModel {
    private String coCode;

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    private String acode;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    private String amount;
    private String paymentMode;

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    private String transactionStatus;
    private String transactionId;
    private String totalAmount;
    private String payPhiRefId;
    private String merchantTxnNo;

    public String getPayPhiRefId() {
        return payPhiRefId;
    }

    public void setPayPhiRefId(String payPhiRefId) {
        this.payPhiRefId = payPhiRefId;
    }

    public String getMerchantTxnNo() {
        return merchantTxnNo;
    }

    public void setMerchantTxnNo(String merchantTxnNo) {
        this.merchantTxnNo = merchantTxnNo;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;
}
