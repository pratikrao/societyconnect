package com.society.connect.model.elocker;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 10/7/17.
 */

public class LockerData {
    @SerializedName("ImageID")
    private String imageID;
    @SerializedName("descr")
    private String descr;
    @SerializedName("filename")
    private String filename;
    @SerializedName("Imgphoto")
    private Object imgphoto;
    @SerializedName("contentType")
    private String contentType;

    public String getImageID() {
        return imageID;
    }


    public void setImageID(String imageID) {
        this.imageID = imageID;
    }


    public String getDescr() {
        return descr;
    }


    public void setDescr(String descr) {
        this.descr = descr;
    }


    public String getFilename() {
        return filename;
    }


    public void setFilename(String filename) {
        this.filename = filename;
    }


    public Object getImgphoto() {
        return imgphoto;
    }


    public void setImgphoto(Object imgphoto) {
        this.imgphoto = imgphoto;
    }

    public String getContentType() {
        return contentType;
    }


    public void setContentType(String contentType) {
        this.contentType = contentType;
    }



}
