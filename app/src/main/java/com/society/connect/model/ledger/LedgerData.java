package com.society.connect.model.ledger;

import com.google.gson.annotations.SerializedName;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerData {
    @SerializedName("creditAmount")
    private String creditAmount;
    @SerializedName("debitAmount")
    private String debitAmount;
    @SerializedName("openingBal")
    private String openingBal;
    @SerializedName("openingCrDr")
    private String openingCrDr;
    @SerializedName("closingBal")
    private String closingBal;
    @SerializedName("closingCrDr")
    private String closingCrDr;

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(String openingBal) {
        this.openingBal = openingBal;
    }

    public String getOpeningCrDr() {
        return openingCrDr;
    }

    public void setOpeningCrDr(String openingCrDr) {
        this.openingCrDr = openingCrDr;
    }

    public String getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(String closingBal) {
        this.closingBal = closingBal;
    }

    public String getClosingCrDr() {
        return closingCrDr;
    }

    public void setClosingCrDr(String closingCrDr) {
        this.closingCrDr = closingCrDr;
    }
}
