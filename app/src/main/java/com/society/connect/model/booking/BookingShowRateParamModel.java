package com.society.connect.model.booking;

/**
 * Created by administrator on 24/6/17.
 */

public class BookingShowRateParamModel {
    private String coCode;
    private String acode;
    private String bookingHead;

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;


    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getBookingHead() {
        return bookingHead;
    }

    public void setBookingHead(String bookingHead) {
        this.bookingHead = bookingHead;
    }
}
