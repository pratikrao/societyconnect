package com.society.connect.model.booking;

/**
 * Created by administrator on 26/6/17.
 */

public class CheckAvailabilityParamModel {
    private String coCode;
    private String acode;
    private String bookingDate;
    private String bookingHead;

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingHead() {
        return bookingHead;
    }

    public void setBookingHead(String bookingHead) {
        this.bookingHead = bookingHead;
    }

    public String getSlot() {

        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    private String slot;

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    private String CID;
}
