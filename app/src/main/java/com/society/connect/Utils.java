package com.society.connect;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pratik on 16/6/16.
 */
public class Utils {
    private static ProgressDialog progressDialog;

    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }


    //To close the Progress Dialog
    public static void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    // To show the Progress dialog
    public static void showProgressDialog(Context ctx) {
        closeProgressDialog();
        progressDialog = ProgressDialog.show(ctx, "", "Please wait", true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    //To convert Array to list
    public static List<String> convertArrayToList(String[] a) {
        return Arrays.asList(a);
    }

    // To combine two Array
    public static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static void setListViewHeight(Adapter adapter, ListView lv) {
        if (adapter.getCount() >= 2) {
            View item = adapter.getView(0, null, lv);
            item.measure(0, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (2 * item.getMeasuredHeight()));
            lv.setLayoutParams(params);
        }
    }

    public static void setDynamicHeight(GridView gridView) {
        ListAdapter gridViewAdapter = gridView.getAdapter();
        if (gridViewAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = gridViewAdapter.getCount();
        int rows = 0;

        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > 3) {
            x = items / 3;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

    //private method of your class
    public static int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static void setScroll(final EditText editText) {
        editText.setMovementMethod(new ScrollingMovementMethod());
        ScrollingMovementMethod.getInstance();
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editText.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    /**
     * To encrypt the characters
     */
    public static String encrypt(String input) {
        final Pattern hidePattern = Pattern.compile("(.{1,2})(.*)(.{1,2})");
        Matcher m = hidePattern.matcher(input);
        String output = null;

        if (m.find()) {
            int l = m.group(2).length();
            output = m.group(1) + new String(new char[l]).replace("\0", "*") + m.group(3);
        }

        return output;
    }

    public static String getCalculatedDate(int days) {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(cal.getTime());
    }

    public static String getFormatedDateAndTime(String dateTime) {
        StringTokenizer stringTokenizer = new StringTokenizer(dateTime);
        String date = stringTokenizer.nextToken();
        String time = stringTokenizer.nextToken();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm");
        SimpleDateFormat simpleAmPmFormat = new SimpleDateFormat("hh:mm a");
        Date dateOne = null;
        try {
            dateOne = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dt;


        SimpleDateFormat simpleMonthFormat = new SimpleDateFormat("MMM d");

        String str = simpleMonthFormat.format(dateOne);
        str = str.substring(0, 1).toUpperCase() + str.substring(1);

        if (dateOne.getDate() > 10 && dateOne.getDate() < 14) {
            str = str + "th, ";
        } else {
            if (str.endsWith("1")) {
                str = str + "st, ";
            } else if (str.endsWith("2")) {
                str = str + "nd, ";
            } else if (str.endsWith("3")) {
                str = str + "rd, ";
            } else {
                str = str + "th, ";
            }
        }

        simpleMonthFormat = new SimpleDateFormat("yyyy");
        str = str + simpleMonthFormat.format(dateOne);
        try {
            dt = simpleTimeFormat.parse(time);
            str = str + " " + simpleAmPmFormat.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

}
