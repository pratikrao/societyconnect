package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.PostComplainParamModel;
import com.society.connect.model.complain_history.ComplainHistoryResponseModel;
import com.society.connect.model.ledger.LedgerModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by administrator on 9/6/17.
 */

public class CreateComplaint extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient {
    EditText edtDetail;
    TextView edtDateOfVisit, edtTime;
    Button btnPost;

    private Calendar calendar;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            edtDateOfVisit.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complaint);
        calendar = Calendar.getInstance();

        edtDetail = (EditText) findViewById(R.id.edtDetail);
        edtDateOfVisit = (TextView) findViewById(R.id.edtDateOfVisit);
        edtTime = (TextView) findViewById(R.id.edtTime);
        btnPost = (Button) findViewById(R.id.btnPost);

        edtDateOfVisit.setOnClickListener(this);
        edtTime.setOnClickListener(this);
        btnPost.setOnClickListener(this);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("NEW COMPLAINT");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtDateOfVisit:
                new DatePickerDialog(CreateComplaint.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.edtTime:
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateComplaint.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        edtTime.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                break;
            case R.id.btnPost:
                if (TextUtils.isEmpty(edtDetail.getText().toString())) {
                    Toast.makeText(CreateComplaint.this, "Enter appropriate detail", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edtDateOfVisit.getText().toString())) {
                    Toast.makeText(CreateComplaint.this, "Select date of visit", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edtTime.getText().toString())) {
                    Toast.makeText(CreateComplaint.this, "Select time", Toast.LENGTH_LONG).show();
                } else {
                    PostComplainParamModel paramModel = new PostComplainParamModel();
                    paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
                    paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
                    paramModel.setComplaintDetails(edtDetail.getText().toString());
                    paramModel.setDateOfVisit(edtDateOfVisit.getText().toString());
                    paramModel.setVisitTime(edtTime.getText().toString());
                    paramModel.setCID(Default.CID);
                    if (NetworkHelper.isOnline(this)) {
                        Utils.showProgressDialog(this);
                        NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                                APIMethod.POST_COMPLAINT, APIRequestcode.POST_COMPLAINT);
                        builder.addParameter("", new Gson().toJson(paramModel));
                        builder.setContentType(NetworkRequest.ContentType.JSON);

                        NetworkRequest networkRequest = builder.build();
                        NetworkJob networkJob = new NetworkJob(this, networkRequest);
                        networkJob.execute();

                    } else {
                        Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            ComplainHistoryResponseModel basicResponseModel = gson.fromJson(reader,
                    ComplainHistoryResponseModel.class);

            if (basicResponseModel.getSuccess()) {
                finishWithSuccess(basicResponseModel.getComplaintNumber());
            } else {
                Toast.makeText(CreateComplaint.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishWithSuccess(String com_no) {

        findViewById(R.id.form_layout).setVisibility(View.GONE);
        findViewById(R.id.successLayout).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_complaint_no)).setText("YOUR COMPLAINT NO\n#" + com_no);
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
