package com.society.connect.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.profile.ProfileDataModel;
import com.society.connect.model.profile.ProfileParamModel;
import com.society.connect.model.social.CategoryDataModel;
import com.society.connect.model.social.CategoryParamModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;

public class Profile extends AppCompatActivity implements BackgroundJobClient {

    TextView fullname, mem_id, flat_no, flat_area, wing, build_no;
    EditText mob_no, email_id;
    Button btn_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        fullname = (TextView) findViewById(R.id.txt_fullname);
        mob_no = (EditText) findViewById(R.id.txt_mobile);
        email_id = (EditText) findViewById(R.id.txt_email);
        mem_id = (TextView) findViewById(R.id.txt_member_id);
        flat_no = (TextView) findViewById(R.id.txt_flatno);
        flat_area = (TextView) findViewById(R.id.txt_flatarea);
        wing = (TextView) findViewById(R.id.txt_wing);
        build_no = (TextView) findViewById(R.id.txt_bldgno);

        btn_update = (Button) findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("PROFILE DETAILS");

        //GET PROFILE DETAILS ON LOAD
        getProfile();

    }

    private void getProfile() {
        //GET PROFILE API REQUEST
        ProfileParamModel.Request paramModel = new ProfileParamModel.Request();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.PROFILE, APIRequestcode.PROFILE);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }




    private void updateProfile() {
        String mobile_no = mob_no.getText().toString();
        String email = email_id.getText().toString();

        if (mobile_no.isEmpty() || email.isEmpty()) {
            Toast.makeText(this, "Mobile no. and Email ID are required", Toast.LENGTH_SHORT).show();
        } else {
            //GET PROFILE API REQUEST
            ProfileParamModel.UpdateRequest paramModel = new ProfileParamModel.UpdateRequest();
            paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
            paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
            paramModel.setCellno(mobile_no);
            paramModel.setEmail(email);
            paramModel.setCID(Default.CID);
            if (NetworkHelper.isOnline(this)) {
                Utils.showProgressDialog(this);
                NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                        APIMethod.PROFILE_UPDATE, APIRequestcode.PROFILE_UPDATE);
                builder.addParameter("", new Gson().toJson(paramModel));
                builder.setContentType(NetworkRequest.ContentType.JSON);

                NetworkRequest networkRequest = builder.build();
                NetworkJob networkJob = new NetworkJob(this, networkRequest);
                networkJob.execute();

            } else {
                Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            switch (item.getItemId()) {
                case android.R.id.home:

                    this.finish();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }

        @Override
        public void onBackgroundJobComplete ( int requestCode, Object result){
            if (requestCode == APIRequestcode.PROFILE) {
                handleProfile(result);
            } else if (requestCode == APIRequestcode.PROFILE_UPDATE) {
                handleProfileUpdate(result);
            }
        }

    private void handleProfileUpdate(Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            final ProfileParamModel.UpdateResponse basicResponseModel = gson.fromJson(reader,
                    ProfileParamModel.UpdateResponse.class);

            Toast.makeText(Profile.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleProfile(Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            final ProfileParamModel.Response basicResponseModel = gson.fromJson(reader,
                    ProfileParamModel.Response.class);

            if (basicResponseModel.getSuccess()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        for (ProfileDataModel da : basicResponseModel.getData()) {
                            fullname.setText(da.getName());
                            mem_id.setText(da.getMemberID());
                            flat_no.setText(da.getFlatno());
                            flat_area.setText(da.getFlatarea());
                            wing.setText(da.getWing());
                            build_no.setText(da.getBldgno());
                            mob_no.setText(da.getCellnumber());
                            email_id.setText(da.getEmailID());
                        }

                    }
                });

            } else {
                Toast.makeText(Profile.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
