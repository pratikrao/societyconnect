package com.society.connect.activity.elocker;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.airpay.airpaysdk_simplifiedotp.ResponseMessage;
import com.airpay.airpaysdk_simplifiedotp.Transaction;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.payphi.customersdk.PaymentOptions;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.online_payment.OnlinePaymentActivity;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.PaymentConfirmationModel;
import com.society.connect.model.booking.BookingHeadParamModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.zip.CRC32;

/**
 * Created by administrator on 10/7/17.
 */

public class ConfirmPaymentActivity extends AppCompatActivity implements BackgroundJobClient, View.OnClickListener {

    TextView txtAmount, txtCharge, txtTotalAmount;
    Button btnConfirm;

    public static String MODE, AMOUNT;
    String transactionId, secureToken, merchantTxnId;

    static String id;

    ResponseMessage resp;
    ArrayList<Transaction> transactionList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_payment);

        txtAmount = (TextView) findViewById(R.id.txtAmount);
        txtCharge = (TextView) findViewById(R.id.txtCharge);
        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);


        getDetails();
        btnConfirm.setOnClickListener(this);
    }

    private void getDetails() {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setAmount(ConfirmPaymentActivity.AMOUNT);
        paramModel.setCID(Default.CID);
        /*paramModel.setPaymentMode(ConfirmPaymentActivity.MODE);
        paramModel.setCID(Default.CID);*/
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.PAYMENT_CONFIRM, APIRequestcode.PAYMENT_CONFIRM);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        switch (requestCode) {
            case APIRequestcode.PAYMENT_CONFIRM:
                try {
                    Gson gson = new Gson();
                    JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                    reader.setLenient(true);

                    PaymentConfirmationModel basicResponseModel = gson.fromJson(reader,
                            PaymentConfirmationModel.class);

                    if (basicResponseModel.getSuccess()) {
                        txtAmount.setText(ConfirmPaymentActivity.AMOUNT);
                        txtCharge.setText(basicResponseModel.getData().getCharges());
                        txtTotalAmount.setText(basicResponseModel.getData().getTotalAmount());
                        transactionId = basicResponseModel.getData().getTransactionId();
                        secureToken = basicResponseModel.getData().getSecureToken();
                        merchantTxnId = basicResponseModel.getData().getMerchantTxnId();
                        id = basicResponseModel.getData().getMerchantTxnId();

                    } else {
                        Toast.makeText(this, "Try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case APIRequestcode.PAYMENT_RESPONSE:
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                reader.setLenient(true);

                PaymentConfirmationModel basicResponseModel = gson.fromJson(reader,
                        PaymentConfirmationModel.class);
                if (respCode != null) {
                    if (respCode.equalsIgnoreCase("000") || respCode.equalsIgnoreCase("0000")) {
                        Toast.makeText(ConfirmPaymentActivity.this, "Payment Successful", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ConfirmPaymentActivity.this, "Payment Failed", Toast.LENGTH_LONG).show();

                    }
                }

                break;


        }

    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onClick(View v) {

      /*  Intent myIntent = new Intent(this, AirpayActivity.class);
        Bundle b = new Bundle();

        Log.e("", "*********** OnCreate Application ********** ");
        //	Toast.makeText(getApplicationContext(),"*********** OnCreate Application ********** ",Toast.LENGTH_LONG).show();

        Log.e("HERE", "" + OnlinePaymentActivity.basicResponseModel.getData().getMid());
        b.putString("USERNAME", OnlinePaymentActivity.basicResponseModel.getData().getUid());
        b.putString("PASSWORD", OnlinePaymentActivity.basicResponseModel.getData().getPwd());
        b.putString("SECRET", OnlinePaymentActivity.basicResponseModel.getData().getSid());
        b.putString("MERCHANT_ID", OnlinePaymentActivity.basicResponseModel.getData().getMid());

        b.putString("EMAIL", OnlinePaymentActivity.basicResponseModel.getData().getEmailid());
        b.putString("PHONE", OnlinePaymentActivity.basicResponseModel.getData().getCellNo());
        b.putString("FIRSTNAME", "");
        b.putString("LASTNAME", "");
        b.putString("ADDRESS", "");
        b.putString("CITY", "");
        b.putString("STATE", "");
        b.putString("COUNTRY", "");
        b.putString("PIN_CODE", "");
        b.putString("CUSTVAR", "air payment");
        b.putString("ORDER_ID", transactionId);
        b.putString("AMOUNT", txtTotalAmount.getText().toString());
        b.putString("MODE", MODE);
        b.putString("CUSTVAR", "air payment");
        b.putString("TXNSUBTYPE", "1");
        b.putString("WALLET", "0");
        b.putString("SUCCESS_URL", "http://www.theroadiesstore.in/airpay/transact/response");

        b.putParcelable("RESPONSEMESSAGE", (Parcelable) resp);


        b.putParcelable("RESPONSEMESSAGE", (Parcelable) resp);

        myIntent.putExtras(b);
        startActivityForResult(myIntent, 120);*/

        Log.v("Details", "" + OnlinePaymentActivity.MERCHANT_ID + "" + OnlinePaymentActivity.EMAIL_ID + secureToken + merchantTxnId+id);
        Intent intent = new Intent(ConfirmPaymentActivity.this, PaymentOptions.class);
        intent.putExtra("Amount", ConfirmPaymentActivity.AMOUNT);
        intent.putExtra("MerchantID", OnlinePaymentActivity.MERCHANT_ID);
        intent.putExtra("MerchantTxnNo", id);
        intent.putExtra("CurrencyCode", 356);
        intent.putExtra("CustomerEmailID", OnlinePaymentActivity.EMAIL_ID);
        intent.putExtra("SecureToken", secureToken);
        startActivityForResult(intent, 1);
    }

    String respCode;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v("DATA", "" + data.getStringExtra("txnID") + "" + data.getStringExtra("responseCode"));
        try {
           /* Bundle bundle = data.getExtras();
            transactionList = new ArrayList<Transaction>();
            transactionList = (ArrayList<Transaction>) bundle.getSerializable("DATA");
            if (transactionList != null) {
                Toast.makeText(this, transactionList.get(0).getSTATUS() + "\n" + transactionList.get(0).getSTATUSMSG(), Toast.LENGTH_LONG).show();


                System.out.println("A=" + transactionList.get(0).getSTATUS()); // 200= success
                System.out.println("B=" + transactionList.get(0).getMERCHANTKEY());
                System.out.println("C=" + transactionList.get(0).getMERCHANTPOSTTYPE());
                System.out.println("D=" + transactionList.get(0).getSTATUSMSG()); //  success or fail
                System.out.println("E=" + transactionList.get(0).getTRANSACTIONAMT());
                System.out.println("F=" + transactionList.get(0).getTXN_MODE());
                System.out.println("G=" + transactionList.get(0).getMERCHANTTRANSACTIONID()); // order id
                System.out.println("H=" + transactionList.get(0).getSECUREHASH());
                System.out.println("I=" + transactionList.get(0).getCUSTOMVAR());
                System.out.println("J=" + transactionList.get(0).getTRANSACTIONID());
                System.out.println("K=" + transactionList.get(0).getTRANSACTIONSTATUS());


                Log.e("11111111111 ", "A=" + transactionList.get(0).getSTATUS()); // 200= success
                Log.e("22222222222 ", "B=" + transactionList.get(0).getMERCHANTKEY());
                Log.e("33333333333 ", "C=" + transactionList.get(0).getMERCHANTPOSTTYPE());
                Log.e("44444444444 ", "D=" + transactionList.get(0).getSTATUSMSG()); //  success or fail
                Log.e("55555555555 ", "E=" + transactionList.get(0).getTRANSACTIONAMT());
                Log.e("66666666666 ", "F=" + transactionList.get(0).getTXN_MODE());
                Log.e("77777777777 ", "G=" + transactionList.get(0).getMERCHANTTRANSACTIONID()); // order id
                Log.e("88888888888 ", "H=" + transactionList.get(0).getSECUREHASH());
                Log.e("99999999999 ", "I=" + transactionList.get(0).getCUSTOMVAR());
                Log.e("00000000000 ", "J=" + transactionList.get(0).getTRANSACTIONID());
                Log.e("45644646646 ", "K=" + transactionList.get(0).getTRANSACTIONSTATUS());


                String transid = transactionList.get(0).getMERCHANTTRANSACTIONID();
                String apTransactionID = transactionList.get(0).getTRANSACTIONID();
                String amount = transactionList.get(0).getTRANSACTIONAMT();
                String transtatus = transactionList.get(0).getTRANSACTIONSTATUS();
                String message = transactionList.get(0).getSTATUSMSG();
                String merchantid = OnlinePaymentActivity.basicResponseModel.getData().getMid(); //provided by Airpay
                String username = OnlinePaymentActivity.basicResponseModel.getData().getUid(); //provided by Airpay;		//provided by Airpay
                String sParam = transid + ":" + apTransactionID + ":" + amount + ":" + transtatus + ":" + message + ":" + merchantid + ":" + username;
                CRC32 crc = new CRC32();
                crc.update(sParam.getBytes());
                String sCRC = "" + crc.getValue();
                System.out.println("Verified Hash= " + sCRC);*/
            respCode = data.getStringExtra("responseCode");
            postSuccess(data.getStringExtra("responseCode"), data.getStringExtra("merchantTxnNo"), data.getStringExtra("txnID"), OnlinePaymentActivity.AMOUNT);


//            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("", "Error Message --- >>> " + e.getMessage());
        }


    }

    private void postSuccess(String status, String txnNo, String id, String amt) {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setTransactionStatus(status);
        paramModel.setMerchantTxnNo(txnNo);
        paramModel.setPayPhiRefId(id);
        paramModel.setTotalAmount(amt);
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.PAYMENT_RESPONSE, APIRequestcode.PAYMENT_RESPONSE);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
}
