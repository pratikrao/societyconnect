package com.society.connect.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.MainActivity;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.LoginRequestParamModel;
import com.society.connect.model.login.LoginModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;


public class LoginActivity extends AppCompatActivity implements BackgroundJobClient {

    private EditText edtName, edtPassword, edtCid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        edtName = (EditText) findViewById(R.id.edtName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtCid = (EditText) findViewById(R.id.edtCid);


        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* startActivity(new Intent(LoginActivity.this, MainActivity.class));
                LoginActivity.this.finish();*/

                login();

            }
        });
    }

    private void login() {
        LoginRequestParamModel loginRequestParamModel = new LoginRequestParamModel();
        loginRequestParamModel.setUserName(edtName.getText().toString());
        loginRequestParamModel.setPassword(edtPassword.getText().toString());
        loginRequestParamModel.setCID(edtCid.getText().toString());
        if (TextUtils.isEmpty(edtName.getText().toString()) || TextUtils.isEmpty(edtPassword.getText().toString()) ||
                TextUtils.isEmpty(edtCid.getText().toString())) {
            Toast.makeText(this, "Please enter all details", Toast.LENGTH_SHORT).show();

        } else {
            if (NetworkHelper.isOnline(this)) {
                Utils.showProgressDialog(this);
                NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                        APIMethod.LOGIN, APIRequestcode.LOGIN);
                builder.addParameter("", new Gson().toJson(loginRequestParamModel));
                builder.setContentType(NetworkRequest.ContentType.JSON);

                NetworkRequest networkRequest = builder.build();
                NetworkJob networkJob = new NetworkJob(this, networkRequest);
                networkJob.execute();

            } else {
                Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);
            LoginModel basicResponseModel = gson.fromJson(reader,
                    LoginModel.class);

            if (basicResponseModel.getSuccess()) {
                Default.CID = edtCid.getText().toString();
                PreferencesManager.getInstance().setBooleanValue(Default.PREF_LOGGED_IN, true);
                PreferencesManager.getInstance().setStringValue(Default.PREF_ACODE, basicResponseModel.getLoginData().getAcode());
                PreferencesManager.getInstance().setIntValue(Default.PREF_COCODE, basicResponseModel.getLoginData().getCoCode());
                PreferencesManager.getInstance().setStringValue(Default.PREF_USERNAME, basicResponseModel.getLoginData().getMemberName());

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                LoginActivity.this.finish();
            } else {
                Toast.makeText(LoginActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
