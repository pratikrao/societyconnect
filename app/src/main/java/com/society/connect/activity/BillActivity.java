package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.BillRequestParamModel;
import com.society.connect.model.bill.BillModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by administrator on 9/6/17.
 */

public class BillActivity extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient {

    TextView txtMemberName, txtMonth, txtBill, txtBillDate, txtCurrentInt, txtArrears, txtTotalAmount, txtFullDetails;
    Button btnView;
    ViewGroup llResult;

    private Calendar calendar;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtMonth.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        calendar = Calendar.getInstance();

        txtMemberName = (TextView) findViewById(R.id.txtMemberName);
        txtMonth = (TextView) findViewById(R.id.txtMonth);
        txtBill = (TextView) findViewById(R.id.txtBill);
        txtBillDate = (TextView) findViewById(R.id.txtBillDate);
        txtCurrentInt = (TextView) findViewById(R.id.txtCurrentInt);
        txtArrears = (TextView) findViewById(R.id.txtArrears);
        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        txtFullDetails = (TextView) findViewById(R.id.txtFullDetails);
        btnView = (Button) findViewById(R.id.btnView);
        llResult = (ViewGroup) findViewById(R.id.llResult);

        txtMemberName.setText(PreferencesManager.getInstance().getStringValue("MEMBER"));
        txtFullDetails.setOnClickListener(this);
        txtMonth.setOnClickListener(this);
        btnView.setOnClickListener(this);
        setTitle("BILL REPORTS");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtMonth:
                new DatePickerDialog(BillActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnView:
                if (TextUtils.isEmpty(txtMonth.getText().toString())) {
                    Toast.makeText(BillActivity.this, "Select Month and Year", Toast.LENGTH_LONG).show();
                } else {
                    BillRequestParamModel paramModel = new BillRequestParamModel();
                    paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
                    paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
                    paramModel.setDate(txtMonth.getText().toString());
                    paramModel.setCID(Default.CID);
                    if (NetworkHelper.isOnline(this)) {
                        Utils.showProgressDialog(this);
                        NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                                APIMethod.SHOW_BILL, APIRequestcode.SHOW_BILL);
                        builder.addParameter("", new Gson().toJson(paramModel));
                        builder.setContentType(NetworkRequest.ContentType.JSON);

                        NetworkRequest networkRequest = builder.build();
                        NetworkJob networkJob = new NetworkJob(this, networkRequest);
                        networkJob.execute();

                    } else {
                        Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.txtFullDetails:
                startActivity(new Intent(BillActivity.this, BillFullActivity.class).putExtra("DATE", txtMonth.getText().toString()));
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            BillModel basicResponseModel = gson.fromJson(reader,
                    BillModel.class);

            if (basicResponseModel.getSuccess()) {
                llResult.setVisibility(View.VISIBLE);
                txtBill.setText(basicResponseModel.getLoginData().getBillNumber());
                txtBillDate.setText(basicResponseModel.getLoginData().getBillDate());
                txtCurrentInt.setText(basicResponseModel.getLoginData().getCurrentInt() + "");
                txtArrears.setText(basicResponseModel.getLoginData().getArrears() + "");
                txtTotalAmount.setText(basicResponseModel.getLoginData().getTotalAmount() + "");
            } else {
                Toast.makeText(BillActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
