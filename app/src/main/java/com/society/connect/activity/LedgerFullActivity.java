package com.society.connect.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.adapter.LedgerAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.LedgerRequestParamModel;
import com.society.connect.model.ledger.LedgerFullData;
import com.society.connect.model.ledger.LedgerFullModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerFullActivity extends AppCompatActivity implements BackgroundJobClient {
    RecyclerView rvLegerDetail;

    String fDate, tDate;
    TextView open_bal, close_bal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_ledger);

        rvLegerDetail = (RecyclerView) findViewById(R.id.rvLegerDetail);
        fDate = getIntent().getStringExtra("FDATE");
        tDate = getIntent().getStringExtra("TDATE");
        fetchDetail();
        open_bal = (TextView) findViewById(R.id.opening_balance);
        close_bal = (TextView) findViewById(R.id.closing_balance);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("LEDGER REPORTS");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fetchDetail() {
        LedgerRequestParamModel paramModel = new LedgerRequestParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue(Default.PREF_COCODE)));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue(Default.PREF_ACODE));
        paramModel.setFromDate(fDate);
        paramModel.setToDate(tDate);
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.LEDGER_GRID, APIRequestcode.LEDGER_GRID);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            LedgerFullModel basicResponseModel = gson.fromJson(reader,
                    LedgerFullModel.class);

            if (basicResponseModel.getSuccess()) {
                if (basicResponseModel.getSuccess()) {
                    ArrayList<LedgerFullData> data = new ArrayList<>();

                    open_bal.setText("OPENING BALANCE\nRs." + basicResponseModel.getOpeningBal()+" "+basicResponseModel.getOpeningCrDr());
                    close_bal.setText("CLOSING BALANCE\nRs." + basicResponseModel.getClosingBal()+" "+basicResponseModel.getClosingCrDr());
                    for (LedgerFullData da : basicResponseModel.getData()) {
                        data.add(da);
                    }
                    if (data.size() > 0) {
                        rvLegerDetail.setLayoutManager(new LinearLayoutManager(rvLegerDetail.getContext()));
                        rvLegerDetail.setAdapter(new LedgerAdapter(LedgerFullActivity.this, data));
                    } else {
                        Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } else {
                    Toast.makeText(LedgerFullActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
