package com.society.connect.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.society.connect.R;

public class ChangePasswordActivity extends AppCompatActivity {

    EditText txt_old_password,txt_new_password,retype_password;
    Button btn_update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        txt_old_password= (EditText) findViewById(R.id.txt_old_password);
        txt_new_password= (EditText) findViewById(R.id.txt_new_password);
        retype_password= (EditText) findViewById(R.id.txt_retype_password);

        btn_update= (Button) findViewById(R.id.btn_change_pswd);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("CHANGE PASSWORD");

    }

    private void changePassword() {
        if(txt_old_password.getText().toString().isEmpty()||txt_new_password.getText().toString().isEmpty()||retype_password.getText().toString().isEmpty()){
            Toast.makeText(this,"ALL FIELDS REQUIRED",Toast.LENGTH_SHORT).show();
        }else if(!txt_new_password.getText().toString().equals(retype_password.getText().toString())){
            Toast.makeText(this,"Password doesn't match",Toast.LENGTH_SHORT).show();
        }else{
            //CHANGE PASSWORD
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
