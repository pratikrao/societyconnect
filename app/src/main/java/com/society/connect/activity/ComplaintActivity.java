package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.adapter.ComplaintAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.complain_history.ComplainHistoryData;
import com.society.connect.model.complain_history.ComplainHistoryParamModel;
import com.society.connect.model.complain_history.ComplainHistoryResponseModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Rafique on 9/6/17.
 */

public class ComplaintActivity extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient, AppBarLayout.OnOffsetChangedListener {
    FloatingActionButton btn_create_complaint, btn_col_exp;
    RecyclerView complaint_list;
    Calendar calendar;
    ViewGroup container_no_data;
    TextView from_date, till_date;
    AppBarLayout appbar;
    DatePickerDialog.OnDateSetListener from_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(java.util.Calendar.YEAR, year);
            calendar.set(java.util.Calendar.MONTH, monthOfYear);
            calendar.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            from_date.setText(sdf.format(calendar.getTime()));
        }
    };
    DatePickerDialog.OnDateSetListener till_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(java.util.Calendar.YEAR, year);
            calendar.set(java.util.Calendar.MONTH, monthOfYear);
            calendar.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            till_date.setText(sdf.format(calendar.getTime()));
        }
    };

    RadioGroup filter;
    RadioButton filterValue;
    Button btn_getlist;
    Boolean appBarIsExpanded = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);
        btn_create_complaint = (FloatingActionButton) findViewById(R.id.btn_create_complaint);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        container_no_data = (ViewGroup) findViewById(R.id.container_no_data);
        btn_col_exp = (FloatingActionButton) findViewById(R.id.btn_col_exp);
        btn_create_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ComplaintActivity.this, CreateComplaint.class));
            }
        });
        btn_col_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ARR", "Clicked fab");
                appbar.setExpanded(appBarIsExpanded, true);
            }
        });
        btn_getlist = (Button) findViewById(R.id.btn_getlist);
        btn_getlist.setOnClickListener(this);
        complaint_list = (RecyclerView) findViewById(R.id.recycler_complaint);
        from_date = (TextView) findViewById(R.id.txt_fromDate);
        from_date.setOnClickListener(this);
        till_date = (TextView) findViewById(R.id.txt_tillDate);
        till_date.setOnClickListener(this);
        calendar = Calendar.getInstance();
        filter = (RadioGroup) findViewById(R.id.radio_filter);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("COMPLAINTS");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_fromDate:
                new DatePickerDialog(ComplaintActivity.this, from_date_listener, calendar
                        .get(java.util.Calendar.YEAR), calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.txt_tillDate:
                new DatePickerDialog(ComplaintActivity.this, till_date_listener, calendar
                        .get(java.util.Calendar.YEAR), calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btn_getlist:
                if (TextUtils.isEmpty(from_date.getText().toString())) {
                    Toast.makeText(ComplaintActivity.this, "Select From Date", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(till_date.getText().toString())) {
                    Toast.makeText(ComplaintActivity.this, "Select To Date", Toast.LENGTH_LONG).show();
                } else {
                    ((AppBarLayout) findViewById(R.id.appbar)).setExpanded(false, true);
                    requestComplaints();
                }
                break;
        }
    }

    private void requestComplaints() {

        ComplainHistoryParamModel paramModel = new ComplainHistoryParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue(Default.PREF_COCODE)));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue(Default.PREF_ACODE));
        paramModel.setFrmDate(from_date.getText().toString());
        paramModel.setToDate(till_date.getText().toString());
        paramModel.setCID(Default.CID);

        if (filter.getCheckedRadioButtonId() == R.id.radio_pending) {
            paramModel.setStat("P");
        } else if (filter.getCheckedRadioButtonId() == R.id.radio_closed) {
            paramModel.setStat("S");
        } else {
            paramModel.setStat("A");
        }

        if (NetworkHelper.isOnline(this)) {
            // Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.VIEW_COMPLAIN_HISTORY, APIRequestcode.VIEW_COMPLAIN_HISTORY);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Log.d("ARR", "onBackgroundJobComplete");

        Utils.closeProgressDialog();
        try {
            Log.d("ARR", "try");

            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            ComplainHistoryResponseModel basicResponseModel = gson.fromJson(reader,
                    ComplainHistoryResponseModel.class);


            if (basicResponseModel.getSuccess()) {
                Log.d("ARR", "basicResponseModel" + basicResponseModel.getSuccess());
                ArrayList<ComplainHistoryData> data = new ArrayList<>();
                Log.d("ARR", "basicResponseModel getData" + basicResponseModel.getData());

                for (ComplainHistoryData da : basicResponseModel.getData()) {
                    data.add(da);
                }
                if (data.size() > 0) {
                    container_no_data.setVisibility(View.GONE);
                    complaint_list.setVisibility(View.VISIBLE);
                    complaint_list.setLayoutManager(new LinearLayoutManager(complaint_list.getContext()));
                    complaint_list.setAdapter(new ComplaintAdapter(ComplaintActivity.this, data));
                } else {
                    container_no_data.setVisibility(View.VISIBLE);
                    complaint_list.setVisibility(View.GONE);
                    Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(ComplaintActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }


            Log.d("ARR", "success");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("ARR", "Exception", e);

        }

    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
        Utils.closeProgressDialog();
        Log.d("ARR", "onBackgroundJobAbort");
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
        Log.d("ARR", "onBackgroundJobError");
        Utils.closeProgressDialog();
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        appbar.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        appbar.removeOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        appBarIsExpanded = (verticalOffset < 0);
        Log.d("ARR","offsetChanged"+verticalOffset);
    }
}
