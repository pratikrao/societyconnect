package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.entry.BookingListActivity;
import com.society.connect.adapter.LedgerAdapter;
import com.society.connect.adapter.RateAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.booking.BookingHeadParamModel;
import com.society.connect.model.booking.BookingHeaderResponseModel;
import com.society.connect.model.booking.BookingPostParamModel;
import com.society.connect.model.booking.BookingShowRateParamModel;
import com.society.connect.model.booking.CheckAvailabilityParamModel;
import com.society.connect.model.booking.CheckAvailabilityResponseModel;
import com.society.connect.model.booking.HeadData;
import com.society.connect.model.booking.PostBookingResponseModel;
import com.society.connect.model.booking.RateData;
import com.society.connect.model.booking.RateResponseModel;
import com.society.connect.model.booking.SlotData;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class BookingActivity extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient, AdapterView.OnItemSelectedListener {
    Spinner booking_type, slots;
    TextView date, check_available, txt_availability;
    Button proceed;
    EditText edtRemark;
    RecyclerView rvRates;

    String codeType, slotType;
    ArrayList<String> code, slot;

    private java.util.Calendar calendar;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(java.util.Calendar.YEAR, year);
            calendar.set(java.util.Calendar.MONTH, monthOfYear);
            calendar.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            date.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        booking_type = (Spinner) findViewById(R.id.spinner_booking_type);
        slots = (Spinner) findViewById(R.id.spinner_booking_slots);
        date = (TextView) findViewById(R.id.txt_booking_date);
        calendar = java.util.Calendar.getInstance();
        check_available = (TextView) findViewById(R.id.btn_check_available);
        proceed = (Button) findViewById(R.id.btn_proceed);
        txt_availability = (TextView) findViewById(R.id.txt_availability);
        edtRemark = (EditText) findViewById(R.id.edtRemark);
        rvRates = (RecyclerView) findViewById(R.id.rvRates);

        proceed.setOnClickListener(this);
        check_available.setOnClickListener(this);
        date.setOnClickListener(this);
        booking_type.setOnItemSelectedListener(this);
        slots.setOnItemSelectedListener(this);


        //CLOSE RATES CARD
        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.container_booking).setVisibility(View.VISIBLE);
                findViewById(R.id.container_rates).setVisibility(View.GONE);
                rvRates.setVisibility(View.GONE);
            }
        });
        //OPEN RATES CARD
        findViewById(R.id.txt_show_rates).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBookingRate();

            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#444444")));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#333333"));
        }
        setTitle("NEW BOOKING");

        findViewById(R.id.btn_booking_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BookingActivity.this, BookingListActivity.class));
            }
        });


        fetchBookingType();

    }

    private void getBookingRate() {
        BookingShowRateParamModel paramModel = new BookingShowRateParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setBookingHead(codeType);
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.BOOKING_RATE, APIRequestcode.BOOKING_RATE);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_booking_date:
                new DatePickerDialog(BookingActivity.this, dateListener, calendar
                        .get(java.util.Calendar.YEAR), calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btn_check_available:
                if (TextUtils.isEmpty(date.getText().toString())) {
                    Toast.makeText(BookingActivity.this, "Select booking date", Toast.LENGTH_LONG).show();
                } else {
                    checkAvailability();
                }
                break;
            case R.id.btn_proceed:
                proceedBooking();
                break;
        }
    }

    private String getBookingTypeValue() {
        switch (booking_type.getSelectedItem().toString()) {
            default:
                return "CLUB HOUSE";
        }
    }

    private void fetchBookingType() {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.BOOKING_HEADER, APIRequestcode.BOOKING_HEADER);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkAvailability() {
        CheckAvailabilityParamModel paramModel = new CheckAvailabilityParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setBookingDate(date.getText().toString());
        paramModel.setBookingHead(codeType);
        paramModel.setSlot(slotType);
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.CHECK_AVAILABILITY, APIRequestcode.CHECK_AVAILABILITY);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void proceedBooking() {
        BookingPostParamModel paramModel = new BookingPostParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setBookingDate(date.getText().toString());
        paramModel.setBookingHead(codeType);
        paramModel.setSlot(slotType);
        paramModel.setRemarks(edtRemark.getText().toString());
        paramModel.setCID(Default.CID);
//        paramModel.setRate("500");
       /* paramModel.setUsername(PreferencesManager.getInstance().getStringValue(Default.PREF_USERNAME));
        paramModel.setStatus("A");*/

        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.POST_BOOKING, APIRequestcode.POST_BOOKING);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        switch (requestCode) {
            case APIRequestcode.BOOKING_HEADER:
                handleBookingHeader(result);
                break;
            case APIRequestcode.BOOKING_RATE:
                handleBookingRate(result);
                break;
            case APIRequestcode.POST_BOOKING:
                handleNewBooking(result);
                break;
            case APIRequestcode.CHECK_AVAILABILITY:
                try {
                    Gson gson = new Gson();
                    JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                    reader.setLenient(true);

                    CheckAvailabilityResponseModel basicResponseModel = gson.fromJson(reader,
                            CheckAvailabilityResponseModel.class);

                    if (basicResponseModel.getSuccess()) {
                        txt_availability.setText(basicResponseModel.getStat());

                        if (basicResponseModel.getStat().equalsIgnoreCase("Available")) {
                            proceed.setVisibility(View.VISIBLE);
                        } else {
                            proceed.setVisibility(View.GONE);
                        }

                    } else {
                        Toast.makeText(this, "Try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


        }

    }

    private void handleNewBooking(Object result) {
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            PostBookingResponseModel basicResponseModel = gson.fromJson(reader,
                    PostBookingResponseModel.class);

            if (basicResponseModel.getSuccess()) {
                Toast.makeText(this, basicResponseModel.getResult(), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(this, basicResponseModel.getResult(), Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleBookingRate(Object result) {
        Log.v("RATE", ((NetworkResponse) result).getResponseString());
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
        reader.setLenient(true);

        RateResponseModel basicResponseModel = gson.fromJson(reader,
                RateResponseModel.class);
        if (basicResponseModel.getSuccess()) {
            findViewById(R.id.container_booking).setVisibility(View.GONE);
            findViewById(R.id.container_rates).setVisibility(View.VISIBLE);
            ArrayList<RateData> data = new ArrayList<>();

            for (RateData d : basicResponseModel.getDATA()) {
                data.add(d);
            }
            rvRates.setLayoutManager(new LinearLayoutManager(rvRates.getContext()));
            rvRates.setAdapter(new RateAdapter(BookingActivity.this, data));

        } else {
            Toast.makeText(BookingActivity.this, "Try again", Toast.LENGTH_SHORT).show();
        }


    }

    private void handleBookingHeader(Object result) {
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            BookingHeaderResponseModel basicResponseModel = gson.fromJson(reader,
                    BookingHeaderResponseModel.class);

            if (basicResponseModel.getSuccess()) {

                if (basicResponseModel.getHeadDATA().size() > 0) {
                    ArrayList<String> data = new ArrayList<>();
                    for (HeadData da : basicResponseModel.getHeadDATA()) {
                        data.add(da.getDescr());
                    }
                    ArrayList<String> slotData = new ArrayList<>();
                    for (SlotData s : basicResponseModel.getSlotDATA()) {
                        slotData.add(s.getSlotName());
                    }
                    slot = new ArrayList<>();
                    for (SlotData s : basicResponseModel.getSlotDATA()) {
                        slot.add(s.getSlotCode());
                    }
                    code = new ArrayList<>();
                    for (HeadData da : basicResponseModel.getHeadDATA()) {
                        code.add(da.getCode());
                    }
                    booking_type.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data));
                    slots.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, slotData));
                } else {
                    Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(BookingActivity.this, basicResponseModel.getResult().toString(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinner_booking_type:
                codeType = code.get(i);
                break;
            case R.id.spinner_booking_slots:
                slotType = slot.get(i);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
