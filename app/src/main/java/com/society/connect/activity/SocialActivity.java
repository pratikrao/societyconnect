package com.society.connect.activity;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.adapter.SocialAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.CategoryResponseModel;
import com.society.connect.model.SocialParamModel;
import com.society.connect.model.booking.BookingPostParamModel;
import com.society.connect.model.social.CategoryDataModel;
import com.society.connect.model.social.CategoryParamModel;
import com.society.connect.model.social.SocialData;
import com.society.connect.model.social.SocialResponseModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;

public class SocialActivity extends AppCompatActivity implements BackgroundJobClient, AppBarLayout.OnOffsetChangedListener, AdapterView.OnItemSelectedListener {
    Spinner category;
    Button getList;
    RecyclerView list;
    ArrayList<CategoryDataModel> data = new ArrayList<>();
    AppBarLayout appbar;
    FloatingActionButton btn_col_exp;
    private boolean appBarIsExpanded = true;
    String mCategoryId;
    ArrayList<String> _spinner_data;

    ArrayList<String> code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        category = (Spinner) findViewById(R.id.spinner_category);
        getList = (Button) findViewById(R.id.btnView);
        list = (RecyclerView) findViewById(R.id.rvList);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        btn_col_exp = (FloatingActionButton) findViewById(R.id.btn_col_exp);
        btn_col_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ARR", "appBarIsExpanded " + appBarIsExpanded);
                appbar.setExpanded(appBarIsExpanded, true);

            }
        });
        getList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getList();
            }
        });

        getCategory();
        category.setOnItemSelectedListener(this);


    }

    private void getCategory() {
        CategoryParamModel paramModel = new CategoryParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setCID(Default.CID);

        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.SOCIAL_CATEGORY, APIRequestcode.SOCIAL_CATEGORY);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getList() {

        SocialParamModel paramModel = new SocialParamModel();
        paramModel.setCo_code(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setCatagory(mCategoryId);
        paramModel.setCID(Default.CID);
//        paramModel.setRate("500");
       /* paramModel.setUsername(PreferencesManager.getInstance().getStringValue(Default.PREF_USERNAME));
        paramModel.setStatus("A");*/

        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.SOCIAL_CATEGORY_DETAILS, APIRequestcode.SOCIAL_CATEGORY_DETAILS);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        switch (requestCode) {
            case APIRequestcode.SOCIAL_CATEGORY:
                handleSocialResponse(result);
                break;
            case APIRequestcode.SOCIAL_CATEGORY_DETAILS:
                try {
                    Gson gson = new Gson();
                    JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                    reader.setLenient(true);

                    final SocialResponseModel basicResponseModel = gson.fromJson(reader,
                            SocialResponseModel.class);

                    if (basicResponseModel.getSuccess()) {

                        final ArrayList<SocialData> datas = new ArrayList<>();
                      /*  runOnUiThread(new Runnable() {
                            @Override
                            public void run() {*/
                        for (SocialData da : basicResponseModel.getData()) {
                            datas.add(da);
                        }


                        if (datas.size() > 0) {
                            ((AppBarLayout) findViewById(R.id.appbar)).setExpanded(false, true);
                            list.setLayoutManager(new LinearLayoutManager(list.getContext()));
                            list.setAdapter(new SocialAdapter(SocialActivity.this, datas));
                        } else {
                            Toast.makeText(SocialActivity.this, "No data", Toast.LENGTH_LONG).show();
                        }
                    }
                       /* });


                    }*/
                    else {
                        Toast.makeText(SocialActivity.this, "Try again", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private void handleSocialResponse(Object result) {

        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            final CategoryResponseModel basicResponseModel = gson.fromJson(reader,
                    CategoryResponseModel.class);

            if (basicResponseModel.getSuccess()) {

                _spinner_data = new ArrayList<>();
                code = new ArrayList<>();

               /* runOnUiThread(new Runnable() {
                    @Override
                    public void run() {*/

                for (CategoryDataModel da : basicResponseModel.getData()) {
                    data.add(da);
                    _spinner_data.add(da.getDescr());

                }
                category.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, _spinner_data));
            }
//                });
//        }
            else {
                Toast.makeText(SocialActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        appbar.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        appbar.removeOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset < 0) {
            appBarIsExpanded = true;
        } else {
            appBarIsExpanded = false;
        }
        Log.d("ARR", "onOffsetChanged" + verticalOffset);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mCategoryId = data.get(position).getCode();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
