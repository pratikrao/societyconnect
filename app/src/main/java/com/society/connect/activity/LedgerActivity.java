package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.model.ledger.LedgerModel;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by administrator on 8/6/17.
 */

public class LedgerActivity extends AppCompatActivity implements View.OnClickListener, BackgroundJobClient {
    //UX Components
    TextView txtOpeningBalance, txtClosingBalance, txtFromDate, txtToDate, txtFullDetails;
    Button btnView;
    ViewGroup llResult;


    private Calendar calendar;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtFromDate.setText(sdf.format(calendar.getTime()));
        }

    };
    DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtToDate.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger);
        calendar = Calendar.getInstance();
        // txtOpeningBalance = (TextView) findViewById(R.id.txtOpeningBalance);
        // txtClosingBalance = (TextView) findViewById(R.id.txtClosingBalance);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        //    txtFullDetails = (TextView) findViewById(R.id.txtFullDetails);
        btnView = (Button) findViewById(R.id.btnView);
        //    llResult = (ViewGroup) findViewById(R.id.llResult);

        txtFromDate.setOnClickListener(this);
        txtToDate.setOnClickListener(this);
        btnView.setOnClickListener(this);
        //    txtFullDetails.setOnClickListener(this);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("LEDGER REPORTS");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtFromDate:
                new DatePickerDialog(LedgerActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.txtToDate:
                new DatePickerDialog(LedgerActivity.this, date1, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnView:
                if (TextUtils.isEmpty(txtFromDate.getText().toString())) {
                    Toast.makeText(LedgerActivity.this, "Select From Date", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(txtToDate.getText().toString())) {
                    Toast.makeText(LedgerActivity.this, "Select To Date", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(new Intent(LedgerActivity.this, LedgerFullActivity.class)
                            .putExtra("FDATE", txtFromDate.getText().toString())
                            .putExtra("TDATE", txtToDate.getText().toString()));
                /*    LedgerRequestParamModel paramModel = new LedgerRequestParamModel();
                    paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
                    paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
                    paramModel.setFromDate(txtFromDate.getText().toString());
                    paramModel.setToDate(txtToDate.getText().toString());
                    if (NetworkHelper.isOnline(this)) {
                        Utils.showProgressDialog(this);
                        NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                                APIMethod.LEDGER, APIRequestcode.LEDGER);
                        builder.addParameter("", new Gson().toJson(paramModel));
                        builder.setContentType(NetworkRequest.ContentType.JSON);

                        NetworkRequest networkRequest = builder.build();
                        NetworkJob networkJob = new NetworkJob(this, networkRequest);
                        networkJob.execute();

                    } else {
                        Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }*/
                }
                break;
            case R.id.txtFullDetails:
                startActivity(new Intent(LedgerActivity.this, LedgerFullActivity.class)
                        .putExtra("FDATE", txtFromDate.getText().toString())
                        .putExtra("TDATE", txtToDate.getText().toString()));
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            LedgerModel basicResponseModel = gson.fromJson(reader,
                    LedgerModel.class);

            if (basicResponseModel.getSuccess()) {
                llResult.setVisibility(View.VISIBLE);
                txtOpeningBalance.setText(basicResponseModel.getLoginData().getOpeningBal() + " " + basicResponseModel.getLoginData().getOpeningCrDr());
                txtClosingBalance.setText(basicResponseModel.getLoginData().getClosingBal() + " " + basicResponseModel.getLoginData().getClosingCrDr());
            } else {
                Toast.makeText(LedgerActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
