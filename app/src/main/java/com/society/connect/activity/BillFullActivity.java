package com.society.connect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.online_payment.OnlinePaymentActivity;
import com.society.connect.adapter.BillGridAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.BillRequestParamModel;
import com.society.connect.model.bill.BillFullData;
import com.society.connect.model.bill.BillFullModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by administrator on 9/6/17.
 */

public class BillFullActivity extends AppCompatActivity implements BackgroundJobClient, View.OnClickListener, AppBarLayout.OnOffsetChangedListener {

    RecyclerView rvBillDetail;
    TextView txtMonth;
    String date;
    Button btnGetDetails;
    FloatingActionButton btn_col_exp;
    AppBarLayout appbar;
    ArrayList<BillFullData> data;
    private boolean appBarIsExpanded = true;
    private Calendar calendar;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtMonth.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_full_details);
        date = getIntent().getStringExtra("DATE");
        rvBillDetail = (RecyclerView) findViewById(R.id.rvBillDetail);
        btnGetDetails = (Button) findViewById(R.id.btnView);
        btnGetDetails.setOnClickListener(this);
        txtMonth = (TextView) findViewById(R.id.txtMonth);
        txtMonth.setOnClickListener(this);
        calendar = Calendar.getInstance();
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        btn_col_exp = (FloatingActionButton) findViewById(R.id.btn_col_exp);
        btn_col_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ARR", "appBarIsExpanded " + appBarIsExpanded);
                appbar.setExpanded(appBarIsExpanded, true);

            }
        });
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        setTitle("BILL DETAILS");

    }

    private void fetchDetail() {
        if (!txtMonth.getText().toString().isEmpty()) {
            BillRequestParamModel paramModel = new BillRequestParamModel();
            paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
            paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
            paramModel.setMonth(txtMonth.getText().toString());
            paramModel.setYear("");
            paramModel.setCID(Default.CID);
            if (NetworkHelper.isOnline(this)) {
                Utils.showProgressDialog(this);
                NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                        APIMethod.BILL_GRID, APIRequestcode.BILL_GRID);
                builder.addParameter("", new Gson().toJson(paramModel));
                builder.setContentType(NetworkRequest.ContentType.JSON);

                NetworkRequest networkRequest = builder.build();
                NetworkJob networkJob = new NetworkJob(this, networkRequest);
                networkJob.execute();

            } else {
                Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            BillFullModel basicResponseModel = gson.fromJson(reader,
                    BillFullModel.class);


            if (basicResponseModel.getSuccess()) {
                data = new ArrayList<>();
                data.add(new BillFullData().setHeadName("Bill No.").setAmount(basicResponseModel.getBillNumber()));
                data.add(new BillFullData().setHeadName("Bill Date").setAmount(basicResponseModel.getBillDate()));
                for (BillFullData da : basicResponseModel.getData()) {
                    data.add(da);
                }
                data.add(new BillFullData().setHeadName("Cur. Interest").setAmount(basicResponseModel.getCurrentInt().toString()));
                data.add(new BillFullData().setHeadName("Cur. Bill Amount").setAmount(basicResponseModel.getTotalrowAmt().toString()));
                data.add(new BillFullData().setHeadName("Arrears").setAmount(basicResponseModel.getArrears().toString()));
                data.add(new BillFullData().setHeadName("Total Amount").setAmount(basicResponseModel.getTotalAmount().toString()));


                if (data.size() > 0) {
                    OnlinePaymentActivity.AMOUNT = basicResponseModel.getTotalAmount() + "";
                    rvBillDetail.setLayoutManager(new LinearLayoutManager(rvBillDetail.getContext()));
                    rvBillDetail.setAdapter(new BillGridAdapter(BillFullActivity.this, data, new BillGridAdapter.OnArticleUserItemClickListener() {
                        @Override
                        public void onArticleUserClick(String id) {

                        }
                    }));
                } else {
                    Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(BillFullActivity.this, basicResponseModel.getResult(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtMonth:
                new DatePickerDialog(BillFullActivity.this, dateListener, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnView:
                if (TextUtils.isEmpty(txtMonth.getText().toString())) {
                    Toast.makeText(BillFullActivity.this, "Select Month and Year", Toast.LENGTH_LONG).show();
                } else {
                    appbar.setExpanded(false, true);
                    fetchDetail();
                }
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bill, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_payment:
                if (data != null && data.size() > 0) {
                    startActivity(new Intent(this, OnlinePaymentActivity.class));
                } else {
                    Toast.makeText(BillFullActivity.this, "Select Appropriate bill to proceed", Toast.LENGTH_SHORT).show();
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        appbar.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        appbar.removeOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset < 0) {
            appBarIsExpanded = true;
        } else {
            appBarIsExpanded = false;
        }
    }
}
