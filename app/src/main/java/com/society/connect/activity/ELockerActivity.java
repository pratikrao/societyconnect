package com.society.connect.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.online_payment.OnlinePaymentActivity;
import com.society.connect.adapter.BillGridAdapter;
import com.society.connect.adapter.ElockerAdapter;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.bill.BillFullData;
import com.society.connect.model.bill.BillFullModel;
import com.society.connect.model.booking.BookingHeadParamModel;
import com.society.connect.model.elocker.ElockerResponseModel;
import com.society.connect.model.elocker.LockerData;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by administrator on 10/7/17.
 */

public class ELockerActivity extends AppCompatActivity implements BackgroundJobClient {

    RecyclerView rvElocker;

    ArrayList<LockerData> data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elocker);

        rvElocker = (RecyclerView) findViewById(R.id.rvElocker);

        setTitle("ELocker");

        getDetails();
    }

    private void getDetails() {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.VIEW_ELOCKER, APIRequestcode.VIEW_ELOCKER);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            ElockerResponseModel basicResponseModel = gson.fromJson(reader,
                    ElockerResponseModel.class);


            if (basicResponseModel.getSuccess()) {
                data = new ArrayList<>();

                for (LockerData da : basicResponseModel.getData()) {
                    data.add(da);
                }

                if (data.size() > 0) {
                    rvElocker.setLayoutManager(new LinearLayoutManager(rvElocker.getContext()));
                    rvElocker.setAdapter(new ElockerAdapter(ELockerActivity.this, data));
                } else {
                    Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(ELockerActivity.this, "Try again", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
