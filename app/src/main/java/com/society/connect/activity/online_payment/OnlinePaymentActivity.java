package com.society.connect.activity.online_payment;



import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import com.payphi.customersdk.Application;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.elocker.ConfirmPaymentActivity;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.ModeData;
import com.society.connect.model.PaymentDetailResposneModel;
import com.society.connect.model.booking.BookingHeadParamModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by administrator on 8/7/17.
 */

public class OnlinePaymentActivity extends AppCompatActivity implements BackgroundJobClient, AdapterView.OnItemSelectedListener, Application.IAppInitializationListener {
    private EditText edtName, edtEmailId, edtPhone, edtAmount, edtAmountPaid;
    private Spinner spnMode;
    public static String AMOUNT;

    ArrayList<ModeData> mode;
    Button btnNext;

    public static PaymentDetailResposneModel basicResponseModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment);
        edtName = (EditText) findViewById(R.id.edtName);
        edtEmailId = (EditText) findViewById(R.id.edtEmailId);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtAmount = (EditText) findViewById(R.id.edtAmount);
        edtAmountPaid = (EditText) findViewById(R.id.edtAmountPaid);
        spnMode = (Spinner) findViewById(R.id.spnMode);
        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtAmountPaid.getText().toString())) {
                    ConfirmPaymentActivity.AMOUNT = edtAmountPaid.getText().toString();
                    startActivity(new Intent(OnlinePaymentActivity.this, ConfirmPaymentActivity.class));

                    /*Intent intent = new Intent(OnlinePaymentActivity.this, PaymentOptions.class);
                    intent.putExtra("Amount",100);
//                    intent.putExtra("MerchantTxnNo ",txnRefNo);
                    intent.putExtra("CurrencyCode", 356);
                    intent.putExtra("MerchantID", MERCHANT_ID);
//                    intent.putExtra("SecureToken",secureToken);
                    intent.putExtra("customerEmailID", edtEmailId.getText().toString());
                    startActivityForResult(intent, 1);*/
                } else {
                    Toast.makeText(OnlinePaymentActivity.this, "Enter Amount to be paid", Toast.LENGTH_SHORT).show();
                }
            }
        });

        getPaymentDetails();
        spnMode.setOnItemSelectedListener(this);


    }

    Application sdkApp;
    public static String MERCHANT_ID;
    public static String EMAIL_ID;

    private void getPaymentDetails() {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);


            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.PAYMENT_DETAILS, APIRequestcode.PAYMENT_DETAILS);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
            reader.setLenient(true);

            basicResponseModel = gson.fromJson(reader,
                    PaymentDetailResposneModel.class);

            if (basicResponseModel.getSuccess()) {
                edtAmount.setText(AMOUNT);
                edtName.setText(basicResponseModel.getData().getMemberName());
                edtEmailId.setText(basicResponseModel.getData().getEmailid());
                edtPhone.setText(basicResponseModel.getData().getCellNo());
                MERCHANT_ID = basicResponseModel.getData().getmId();
                EMAIL_ID = basicResponseModel.getData().getEmailid();
                sdkApp = new Application();
                sdkApp.setEnv(Application.PROD);
                Log.v("MID",basicResponseModel.getData().getmId());
                sdkApp.setAppInfo(basicResponseModel.getData().getAggregatorID(), basicResponseModel.getData().getAppId(),
                        getApplicationContext(), new Application.IAppInitializationListener() {
                            @Override
                            public void onSuccess(String s) {
                                Log.v("S",""+s);
                            }

                            @Override
                            public void onFailure(String s) {
                                Log.v("F",""+s);
                            }
                        });
                ArrayList<String> data = new ArrayList<>();
                mode = new ArrayList<>();
                for (ModeData da : basicResponseModel.getMode()) {
                    mode.add(da);
                    data.add(da.getDescr());
                }
                spnMode.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data));

//                sdkApp.setMerchantName("Test", getApplicationContext());
            } else {
                Toast.makeText(this, "Try again", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ConfirmPaymentActivity.MODE = mode.get(position).getModeCode();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onSuccess(String s) {
        Log.v("success","success");
    }

    @Override
    public void onFailure(String s) {
        Log.v("success","failure");
    }
}
