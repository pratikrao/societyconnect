package com.society.connect.activity.entry;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.society.connect.Default;
import com.society.connect.PreferencesManager;
import com.society.connect.R;
import com.society.connect.Utils;
import com.society.connect.activity.BookingActivity;
import com.society.connect.activity.LedgerFullActivity;
import com.society.connect.adapter.BookingAdapter;
import com.society.connect.adapter.LedgerAdapter;
import com.society.connect.classes.BookingDataPattern;
import com.society.connect.controller.APIMethod;
import com.society.connect.controller.APIRequestcode;
import com.society.connect.helper.NetworkHelper;
import com.society.connect.model.LedgerRequestParamModel;
import com.society.connect.model.booking.BookingHeadParamModel;
import com.society.connect.model.booking.BookingHeaderResponseModel;
import com.society.connect.model.booking.BookingPostParamModel;
import com.society.connect.model.booking.HeadData;
import com.society.connect.model.booking.view_booking.ViewBookingData;
import com.society.connect.model.booking.view_booking.ViewBookingParamModel;
import com.society.connect.model.booking.view_booking.ViewBookingResponseModel;
import com.society.connect.model.ledger.LedgerFullData;
import com.society.connect.model.ledger.LedgerFullModel;
import com.society.connect.network.NetworkJob;
import com.society.connect.network.NetworkRequest;
import com.society.connect.network.NetworkResponse;
import com.society.connect.threading.BackgroundJobClient;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class BookingListActivity extends AppCompatActivity implements BackgroundJobClient, View.OnClickListener, AdapterView.OnItemSelectedListener {
    private RecyclerView listview_menu;
    private TextView txtDate;
    private Button btn_booking;
    private BookingAdapter adapter;
    private ArrayList<ViewBookingData> data;
    Spinner s;
    String codeType;
    ArrayList<String> code;
    AppBarLayout appbar;
    ViewGroup viewdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);
        txtDate = (TextView) findViewById(R.id.txtDate);
        btn_booking = (Button) findViewById(R.id.btn_booking);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        viewdata = (ViewGroup) findViewById(R.id.data);
        listview_menu = (RecyclerView) findViewById((R.id.listview_menu));

        calendar = java.util.Calendar.getInstance();
        if (getActionBar() != null) {
            getActionBar().setTitle("BOOKING LIST");
        }


        //SPINNER DATA
        String[] arraySpinner = new String[]{
                "CLUB HOUSE", "GARDEN", "PARKING", "GYM"
        };
        s = (Spinner) findViewById(R.id.spinner_head);
        getHeadContent();

        txtDate.setOnClickListener(this);
        s.setOnItemSelectedListener(this);
        btn_booking.setOnClickListener(this);

    }

    private void getHeadContent() {
        BookingHeadParamModel paramModel = new BookingHeadParamModel();
        paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue(Default.PREF_COCODE)));
        paramModel.setAcode(PreferencesManager.getInstance().getStringValue(Default.PREF_ACODE));
        paramModel.setCID(Default.CID);
        if (NetworkHelper.isOnline(this)) {
            Utils.showProgressDialog(this);
            NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                    APIMethod.BOOKING_HEADER, APIRequestcode.BOOKING_HEADER);
            builder.addParameter("", new Gson().toJson(paramModel));
            builder.setContentType(NetworkRequest.ContentType.JSON);

            NetworkRequest networkRequest = builder.build();
            NetworkJob networkJob = new NetworkJob(this, networkRequest);
            networkJob.execute();

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Utils.closeProgressDialog();
        if (requestCode == APIRequestcode.BOOKING_HEADER) {
            try {
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                reader.setLenient(true);

                BookingHeaderResponseModel basicResponseModel = gson.fromJson(reader,
                        BookingHeaderResponseModel.class);

                if (basicResponseModel.getSuccess()) {
                    if (basicResponseModel.getSuccess()) {
                        ArrayList<String> arraySpinner = new ArrayList<>();

                        for (HeadData da : basicResponseModel.getHeadDATA()) {
                            arraySpinner.add(da.getDescr());
                        }
                        code = new ArrayList<>();
                        for (HeadData da : basicResponseModel.getHeadDATA()) {
                            code.add(da.getCode());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                                android.R.layout.simple_list_item_1, arraySpinner);
                        s.setAdapter(adapter);
                    } else {
                        Toast.makeText(BookingListActivity.this, basicResponseModel.getResult().toString(), Toast.LENGTH_LONG).show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(((NetworkResponse) result).getResponseString()));
                reader.setLenient(true);

                ViewBookingResponseModel basicResponseModel = gson.fromJson(reader,
                        ViewBookingResponseModel.class);


                if (basicResponseModel.getSuccess()) {
                    //POPULATE DETAILS
                    data = new ArrayList<>();
                    for (ViewBookingData data1 : basicResponseModel.getData()) {
                        data.add(data1);
                    }

                    if (data.size() > 0) {
                        listview_menu.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        listview_menu.setAdapter(new BookingAdapter(this, data));
                    } else {
                        Toast.makeText(BookingListActivity.this, "No Data", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(BookingListActivity.this, basicResponseModel.getResult().toString(), Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                new DatePickerDialog(BookingListActivity.this, dateListener, calendar
                        .get(java.util.Calendar.YEAR), calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btn_booking:
                if (TextUtils.isEmpty(txtDate.getText().toString())) {
                    Toast.makeText(BookingListActivity.this, "Select  date", Toast.LENGTH_LONG).show();
                } else {
                    appbar.setExpanded(false, true);
                    ViewBookingParamModel paramModel = new ViewBookingParamModel();
                    paramModel.setCoCode(String.valueOf(PreferencesManager.getInstance().getIntValue("COCODE")));
                    paramModel.setAcode(PreferencesManager.getInstance().getStringValue("ACODE"));
                    paramModel.setBookingHead(codeType);
                    paramModel.setCID(Default.CID);


                    if (NetworkHelper.isOnline(this)) {
                        Utils.showProgressDialog(this);
                        NetworkRequest.Builder builder = new NetworkRequest.Builder(NetworkRequest.MethodType.POST,
                                APIMethod.VIEW_BOOKING, APIRequestcode.VIEW_BOOKING);
                        builder.addParameter("", new Gson().toJson(paramModel));
                        builder.setContentType(NetworkRequest.ContentType.JSON);

                        NetworkRequest networkRequest = builder.build();
                        NetworkJob networkJob = new NetworkJob(this, networkRequest);
                        networkJob.execute();

                    } else {
                        Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private java.util.Calendar calendar;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(java.util.Calendar.YEAR, year);
            calendar.set(java.util.Calendar.MONTH, monthOfYear);
            calendar.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            txtDate.setText(sdf.format(calendar.getTime()));
        }

    };

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        codeType = code.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
