package com.society.connect.adapter;

/**
 * Created by mohammed.rafique on 6/24/2017.
 */


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.model.bill.BillFullData;
import com.society.connect.model.complain_history.ComplainHistoryData;

import java.util.ArrayList;

/**
 * Created by administrator on 9/6/17.
 */

public class ComplaintAdapter extends RecyclerView.Adapter<ComplaintAdapter.ViewHolder> {
    public OnArticleUserItemClickListener mOnArticleUserItemClickListener;
    ArrayList<ComplainHistoryData> menuModels;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ComplaintAdapter(Context context, ArrayList<ComplainHistoryData> myDataset, OnArticleUserItemClickListener mOnArticleUserItemClickListener) {
        this.context = context;
        menuModels = myDataset;
        this.mOnArticleUserItemClickListener = mOnArticleUserItemClickListener;
    }  // Provide a suitable constructor (depends on the kind of dataset)

    public ComplaintAdapter(Context context, ArrayList<ComplainHistoryData> myDataset) {
        this.context = context;
        menuModels = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ComplaintAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vhh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_complaint_history, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ComplaintAdapter.ViewHolder(vhh);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ComplaintAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

         ComplainHistoryData menuData = menuModels.get(position);
        holder.txt_com_no.setText(menuData.getComplaintNo());
        if (menuData.getStat().equals("P")) {
            holder.txt_com_status.setTextColor(Color.parseColor("#ff0000"));
            holder.txt_com_status.setText("PENDING");
        } else {
            holder.txt_com_status.setText("CLOSED");

        }
        holder.txt_com_solved_on.setText(menuData.getSolvedDt());
        holder.txt_com_solved_by.setText(menuData.getSolvedBy());
        holder.txt_com_remarks.setText(menuData.getRemark());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuModels.size();
    }


    public interface OnArticleUserItemClickListener {
        void onArticleUserClick(String id);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txt_com_no, txt_com_status, txt_com_solved_on, txt_com_solved_by, txt_com_remarks;


        public ViewHolder(View v) {
            super(v);
            txt_com_no = (TextView) v.findViewById(R.id.txt_com_no);
            txt_com_status = (TextView) v.findViewById(R.id.txt_com_status);
            txt_com_solved_on = (TextView) v.findViewById(R.id.txt_com_solved_on);
            txt_com_solved_by = (TextView) v.findViewById(R.id.txt_com_solved_by);
            txt_com_remarks = (TextView) v.findViewById(R.id.txt_com_remarks);
        }
    }
}

