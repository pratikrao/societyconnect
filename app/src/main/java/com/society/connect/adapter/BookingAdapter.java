package com.society.connect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.classes.BookingDataPattern;
import com.society.connect.model.booking.view_booking.ViewBookingData;

import java.util.ArrayList;

/**
 * Created by mohammed.rafique on 26/4/2017.
 */

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.BookingViewHolder> {
    Context ctx;
    private ArrayList<ViewBookingData> data;

    public BookingAdapter(Context _c, ArrayList<ViewBookingData> _data) {
        ctx = _c;
        data = _data;

    }

    public BookingAdapter(Context _c) {
        ctx = _c;
    }

    @Override
    public BookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(ctx).inflate(R.layout.vh_booking_details, parent, false);
        return new BookingViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(BookingViewHolder holder, int position) {
        holder.bookedby.setText(data.get(position).getDate());
        holder.status.setText(data.get(position).getStat());
        holder.remarks.setText(data.get(position).getRemark());
        holder.txtSlot.setText(data.get(position).getSlotd());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<ViewBookingData> data) {
        this.data = data;
    }

    public class BookingViewHolder extends RecyclerView.ViewHolder {

        public TextView bookedby, status, remarks,txtSlot;

        public BookingViewHolder(View itemView) {
            super(itemView);
            bookedby = (TextView) itemView.findViewById(R.id.txt_booked_by);
            status = (TextView) itemView.findViewById(R.id.txt_booked_status);
            remarks = (TextView) itemView.findViewById(R.id.txt_booked_remarks);
            txtSlot = (TextView) itemView.findViewById(R.id.txtSlot);

        }

    }
}
