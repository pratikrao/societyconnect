package com.society.connect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.model.booking.RateData;
import com.society.connect.model.social.SocialData;

import java.util.ArrayList;

/**
 * Created by administrator on 5/7/17.
 */

public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.ViewHolder> {
    ArrayList<SocialData> menuModels;
    private Context context;

    public SocialAdapter(Context context, ArrayList<SocialData> myDataset) {
        this.context = context;
        menuModels = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SocialAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vhh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_social, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new SocialAdapter.ViewHolder(vhh);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SocialAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SocialData menuData = menuModels.get(position);
        holder.txt_company.setText(menuData.getName());
        holder.txt_phone.setText(menuData.getCellNo());
        holder.txt_contact_person.setText(menuData.getContactPerson());
        holder.txt_address.setText(menuData.getAddress());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuModels.size();
    }


    public interface OnArticleUserItemClickListener {
        void onArticleUserClick(String id);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txt_company, txt_contact_person, txt_address, txt_phone;


        public ViewHolder(View v) {
            super(v);
            txt_company = (TextView) v.findViewById(R.id.txt_company);
            txt_contact_person = (TextView) v.findViewById(R.id.txt_contact_person);
            txt_address = (TextView) v.findViewById(R.id.txt_address);
            txt_phone = (TextView) v.findViewById(R.id.txt_phone);

        }
    }
}


