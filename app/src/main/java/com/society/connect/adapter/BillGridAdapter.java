package com.society.connect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.model.bill.BillFullData;

import java.util.ArrayList;

/**
 * Created by administrator on 9/6/17.
 */

public class BillGridAdapter extends RecyclerView.Adapter<BillGridAdapter.ViewHolder> {
    public OnArticleUserItemClickListener mOnArticleUserItemClickListener;
    ArrayList<BillFullData> menuModels;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public BillGridAdapter(Context context, ArrayList<BillFullData> myDataset, OnArticleUserItemClickListener mOnArticleUserItemClickListener) {
        this.context = context;
        menuModels = myDataset;
        this.mOnArticleUserItemClickListener = mOnArticleUserItemClickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BillGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vhh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_bill, parent, false);
        // set the view's size, margins, paddings and layout parameters
        BillGridAdapter.ViewHolder vh = new BillGridAdapter.ViewHolder(vhh);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(BillGridAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final BillFullData menuData = menuModels.get(position);
        holder.txtHeading.setText(menuData.getHeadName());
        holder.txtAmount.setText(menuData.getAmount() + "");


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuModels.size();
    }


    public interface OnArticleUserItemClickListener {
        void onArticleUserClick(String id);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeading, txtAmount;


        public ViewHolder(View v) {
            super(v);
            txtHeading = (TextView) v.findViewById(R.id.txtHeading);
            txtAmount = (TextView) v.findViewById(R.id.txtAmount);
        }
    }
}
