package com.society.connect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.society.connect.R;
import com.society.connect.model.booking.view_booking.ViewBookingData;
import com.society.connect.model.elocker.LockerData;

import java.util.ArrayList;

/**
 * Created by administrator on 10/7/17.
 */

public class ElockerAdapter extends RecyclerView.Adapter<ElockerAdapter.BookingViewHolder> {
    Context ctx;
    private ArrayList<LockerData> data;

    public ElockerAdapter(Context _c, ArrayList<LockerData> _data) {
        ctx = _c;
        data = _data;

    }

    public ElockerAdapter(Context _c) {
        ctx = _c;
    }

    @Override
    public ElockerAdapter.BookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(ctx).inflate(R.layout.row_elocler, parent, false);
        return new ElockerAdapter.BookingViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(ElockerAdapter.BookingViewHolder holder, int position) {
        holder.txtName.setText(data.get(position).getDescr());

        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ctx, "To view this doc visit portal", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<LockerData> data) {
        this.data = data;
    }

    public class BookingViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName;

        public BookingViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);



        }

    }
}

