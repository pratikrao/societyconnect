package com.society.connect.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.society.connect.PaymentIntimationActivity;
import com.society.connect.R;
import com.society.connect.activity.BillFullActivity;
import com.society.connect.activity.BookingActivity;
import com.society.connect.activity.ComplaintActivity;
import com.society.connect.activity.LedgerActivity;
import com.society.connect.activity.entry.BookingListActivity;
import com.society.connect.classes.BasicData;

import java.util.ArrayList;

/**
 * Created by mohammed.rafique on 26/4/2017.
 */

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.SubMenuViewHolder> {
    Context ctx;
    private ArrayList<BasicData> submenus;

    public SubMenuAdapter(Context _c, ArrayList<BasicData> _submenus) {
        ctx = _c;
        submenus = _submenus;

    }

    @Override
    public SubMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(ctx).inflate(R.layout.vh_submenu, parent, false);
        return new SubMenuViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(SubMenuViewHolder holder, final int position) {
        holder.setTitle(submenus.get(position).getTitle());
        holder.setIcon(submenus.get(position).getIcon());
        if (submenus.get(position).getAccent_color() != null) {
            holder.container.setBackgroundColor(Color.parseColor(submenus.get(position).getAccent_color()));
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (submenus.get(position).getTitle().equalsIgnoreCase("Complaints")) {
                                                        ctx.startActivity(new Intent(ctx, ComplaintActivity.class));
                                                    } else if (submenus.get(position).getTitle().equalsIgnoreCase("Bill")) {
                                                        ctx.startActivity(new Intent(ctx, BillFullActivity.class));
                                                    } else if (submenus.get(position).getTitle().equalsIgnoreCase("Ledger")) {
                                                        ctx.startActivity(new Intent(ctx, LedgerActivity.class));
                                                    } else if (submenus.get(position).getTitle().equalsIgnoreCase("View Booking") ||
                                                            submenus.get(position).getTitle().equalsIgnoreCase("Booking History")) {
                                                        ctx.startActivity(new Intent(ctx, BookingListActivity.class));
                                                    } else if (submenus.get(position).getTitle().equalsIgnoreCase("Booking")) {
                                                        ctx.startActivity(new Intent(ctx, BookingActivity.class));
                                                    } else if (submenus.get(position).getTitle().equalsIgnoreCase("Payment Intimation")) {
                                                        ctx.startActivity(new Intent(ctx, PaymentIntimationActivity.class));
                                                    } else {
                                                        Toast.makeText(ctx, "Coming Soon", Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            }

        );
    }

    @Override
    public int getItemCount() {
        return submenus.size();
    }

    public class SubMenuViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView icon;
        private View container;

        public SubMenuViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txt_title);
            icon = (ImageView) itemView.findViewById(R.id.img_icon);
            container = itemView.findViewById(R.id.contianer);
            /*container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx, BookingListActivity.class));
                }
            });*/
        }

        public String getTitle() {
            return title.getText().toString();
        }

        public void setTitle(String title) {
            this.title.setText(title);
        }

        public ImageView getIcon() {
            return icon;
        }

        public void setIcon(int icon) {
            this.icon.setImageDrawable(ContextCompat.getDrawable(ctx, icon));
        }
    }
}
