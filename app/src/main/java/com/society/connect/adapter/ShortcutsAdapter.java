package com.society.connect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.society.connect.R;
import com.society.connect.activity.BillFullActivity;
import com.society.connect.activity.BookingActivity;
import com.society.connect.activity.ComplaintActivity;
import com.society.connect.activity.ELockerActivity;
import com.society.connect.activity.LedgerActivity;
import com.society.connect.activity.SocialActivity;
import com.society.connect.activity.online_payment.OnlinePaymentActivity;
import com.society.connect.classes.BasicData;

import java.util.ArrayList;

/**
 * Created by mohammed.rafique on 26/4/2017.
 */

public class ShortcutsAdapter extends RecyclerView.Adapter<ShortcutsAdapter.ShortcutViewHolder> {
    Context ctx;
    private ArrayList<BasicData> shortcutData;
    private Boolean transparent;

    public ShortcutsAdapter(Context _c, ArrayList<BasicData> _d) {
        ctx = _c;
        shortcutData = _d;
        transparent = false;
    }

    public ShortcutsAdapter(Context _c) {
        ctx = _c;
        transparent = false;
        shortcutData = new ArrayList<>();
        shortcutData.add(new BasicData("Complaint").setIcon(R.drawable.ic_comment_white_36dp));
        shortcutData.add(new BasicData("Bill Reports").setIcon(R.drawable.ic_assessment_white_36dp));
        shortcutData.add(new BasicData("E-LOCKER").setIcon(R.drawable.ic_lock_white_36dp));
        shortcutData.add(new BasicData("SOCIAL").setIcon(R.drawable.ic_group_white_36dp));
        shortcutData.add(new BasicData("Booking").setIcon(R.drawable.ic_check_circle_white_36dp));
        shortcutData.add(new BasicData("Ledger").setIcon(R.drawable.ic_date_range_white_36dp));
//        shortcutData.add(new BasicData("Online Payment").setIcon(R.drawable.ic_payment_white_36dp));
    }

    @Override
    public ShortcutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflater;
        if (transparent) {
            inflater = LayoutInflater.from(ctx).inflate(R.layout.vh_shortcut_transparent, parent, false);
        } else {
            inflater = LayoutInflater.from(ctx).inflate(R.layout.vh_shortcut, parent, false);
        }
        return new ShortcutViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(ShortcutViewHolder holder, final int position) {
        holder.setTitle(shortcutData.get(position).getTitle());
        holder.setIcon(shortcutData.get(position).getIcon());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shortcutData.get(position).getTitle().equalsIgnoreCase("Ledger")) {
                    ctx.startActivity(new Intent(ctx, LedgerActivity.class));
                } else if (shortcutData.get(position).getTitle().equalsIgnoreCase("Complaint")) {
                    ctx.startActivity(new Intent(ctx, ComplaintActivity.class));
                } else if (shortcutData.get(position).getTitle().equalsIgnoreCase("Bill Reports")) {
                    ctx.startActivity(new Intent(ctx, BillFullActivity.class));
                } else if (shortcutData.get(position).getTitle().equalsIgnoreCase("E-LOCKER")) {
                    ctx.startActivity(new Intent(ctx, ELockerActivity.class));
                } else if (shortcutData.get(position).getTitle().equalsIgnoreCase("SOCIAL")) {
                    ctx.startActivity(new Intent(ctx, SocialActivity.class));
                } else if (shortcutData.get(position).getTitle().equalsIgnoreCase("Booking")) {
                    ctx.startActivity(new Intent(ctx, BookingActivity.class));

                }else if (shortcutData.get(position).getTitle().equalsIgnoreCase("Online Payment")) {
                    ctx.startActivity(new Intent(ctx, OnlinePaymentActivity.class));

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return shortcutData.size();
    }

    public void setTransparent(boolean transparent) {
        this.transparent = transparent;
    }

    public class ShortcutViewHolder extends RecyclerView.ViewHolder {

        public View container;
        private TextView title;
        private ImageView icon;

        public ShortcutViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txt_title);
            icon = (ImageView) itemView.findViewById(R.id.img_icon);
            container = itemView.findViewById(R.id.container);
        }

        public void setTitle(String title) {
            this.title.setText(title);
        }


        public void setIcon(int icon) {
            this.icon.setImageDrawable(ContextCompat.getDrawable(ctx, icon));
        }
    }
}
