package com.society.connect.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.model.booking.RateData;
import com.society.connect.model.complain_history.ComplainHistoryData;

import java.util.ArrayList;

/**
 * Created by administrator on 3/7/17.
 */

public class RateAdapter extends RecyclerView.Adapter<RateAdapter.ViewHolder> {
    ArrayList<RateData> menuModels;
    private Context context;

    public RateAdapter(Context context, ArrayList<RateData> myDataset) {
        this.context = context;
        menuModels = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vhh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_rate, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new RateAdapter.ViewHolder(vhh);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        RateData menuData = menuModels.get(position);
        holder.txtName.setText(menuData.getSlotName());
        holder.txtRate.setText(menuData.getRate());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuModels.size();
    }


    public interface OnArticleUserItemClickListener {
        void onArticleUserClick(String id);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtName, txtRate;


        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtRate = (TextView) v.findViewById(R.id.txtRate);

        }
    }
}


