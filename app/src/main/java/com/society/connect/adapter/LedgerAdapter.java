package com.society.connect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.society.connect.R;
import com.society.connect.model.ledger.LedgerFullData;

import java.util.ArrayList;

/**
 * Created by administrator on 9/6/17.
 */

public class LedgerAdapter extends RecyclerView.Adapter<LedgerAdapter.ViewHolder> {
    public OnArticleUserItemClickListener mOnArticleUserItemClickListener;
    ArrayList<LedgerFullData> menuModels;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public LedgerAdapter(Context context, ArrayList<LedgerFullData> myDataset) {
        this.context = context;
        menuModels = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LedgerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vhh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_leger, parent, false);
        // set the view's size, margins, paddings and layout parameters
        LedgerAdapter.ViewHolder vh = new LedgerAdapter.ViewHolder(vhh);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(LedgerAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final LedgerFullData menuData = menuModels.get(position);
        if (menuData.getDocNo() != null || !menuData.getDocNo().equalsIgnoreCase(" ")) {
            holder.txtDocNo.setText(menuData.getDocNo());
        } else {
            holder.txtDocNo.setText(" - ");
        }
        if (menuData.getDocDate() != null || !menuData.getDocDate().equalsIgnoreCase(" ")) {
            holder.txtDocDate.setText(menuData.getDocDate());
        } else {
            holder.txtDocDate.setText(" - ");
        }
        if (menuData.getDebit() != null || !menuData.getDebit().equalsIgnoreCase(" ")) {
            holder.txtDebit.setText(menuData.getDebit());
        } else {
            holder.txtDebit.setText(" - ");
        }
        if (menuData.getCredit() != null || !menuData.getCredit().equalsIgnoreCase(" ")) {
            holder.txtCredit.setText(menuData.getCredit());
        } else {
            holder.txtCredit.setText(" - ");
        }
        if (menuData.getType() != null || !menuData.getType().equalsIgnoreCase(" ")) {
            holder.txtType.setText(menuData.getType());
        } else {
            holder.txtType.setText(" - ");
        }
        if (menuData.getChqno() != null || !menuData.getChqno().equalsIgnoreCase(" ")) {
            holder.txtChequeNo.setText(menuData.getChqno());
        } else {
            holder.txtChequeNo.setText(" - ");
        }
        if (menuData.getDescr() != null || !menuData.getDescr().equalsIgnoreCase(" ")) {
            holder.txtDesc.setText(menuData.getDescr());
        } else {
            holder.txtDesc.setText(" - ");
        }
        holder.txtEntry.setText("" + (position + 1) + " :- ");

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuModels.size();
    }


    public interface OnArticleUserItemClickListener {
        void onArticleUserClick(String id);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtDocNo, txtDocDate, txtDebit, txtCredit, txtType, txtChequeNo, txtDesc, txtEntry;


        public ViewHolder(View v) {
            super(v);
            txtDocNo = (TextView) v.findViewById(R.id.txtDocNo);
            txtDocDate = (TextView) v.findViewById(R.id.txtDocDate);
            txtDebit = (TextView) v.findViewById(R.id.txtDebit);
            txtCredit = (TextView) v.findViewById(R.id.txtCredit);
            txtType = (TextView) v.findViewById(R.id.txtType);
            txtChequeNo = (TextView) v.findViewById(R.id.txtChequeNo);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtEntry = (TextView) v.findViewById(R.id.txtEntry);


        }
    }
}

